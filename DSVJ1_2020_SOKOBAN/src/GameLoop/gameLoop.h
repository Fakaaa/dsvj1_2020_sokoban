#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "Screen/screen.h"
#include "GameHandle/gameHandle.h"

using namespace Sokoban;
using namespace ScreenHandle;
using namespace GameHandle;

namespace Sokoban {
	namespace GameLoop {
		//-------------
		class Game
		{
		private:
			Screen* _window;
			GameHanlde* _gameFlow;
		public:
			Game();
			~Game();
			void Play();
		};
		//-------------
	}
}
#endif // !GAMELOOP_H