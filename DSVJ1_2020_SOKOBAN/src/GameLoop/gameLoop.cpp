#include "gameLoop.h"

namespace Sokoban {
	namespace GameLoop {
		//---------------
		Game::Game()
		{
			_window = new Screen(1240,720,"Escape-The-Dungeon! - v0.3.0");
			_gameFlow = new GameHanlde(_window);
		}
		//---------------
		Game::~Game()
		{
			if (_window != NULL)
				delete _window;
			if (_gameFlow != NULL)
				delete _gameFlow;
		}
		//---------------
		static void startDraw() {
			//------
			BeginDrawing();
			ClearBackground(BLACK);
			//------
		}
		//---------------
		static void endDraw() {
			//------
			EndDrawing();
			//------
		}
		//---------------
		void Game::Play()
		{
			_gameFlow->initAll(_window);

			while (!WindowShouldClose() && _gameFlow->getGameClosed())
			{
				startDraw();
				
				_gameFlow->inputs();

				_gameFlow->updateAll(_window);

				_gameFlow->drawAll();

				endDraw();
			}

			_gameFlow->deinitAll();
		}
		//---------------
	}
}