#include "gameHandle.h"

namespace Sokoban {
	namespace GameHandle {
		//---------------------------
		GameHanlde::GameHanlde(Screen* window)
		{
			for (int i = 0; i < cantMaxMenus; i++)
			{
				_UI_Menus[cantMaxMenus] = NULL;
			}

			InitAudioDevice();

			_gameplaySong = NULL;
			_menuSong = NULL;
			_playerMove = NULL;
			for (int i = 0; i < menuSounds; i++) { _menuSfx[i] = NULL; }

			_gameplaySong = new MusicController("res/assets/Music/gameplaySong.mp3");
			_menuSong = new MusicController("res/assets/Music/menuSong.mp3");

			_playerMove = new SoundsController("res/assets/Sounds/playerMove.ogg");
			_menuSfx[Click] = new SoundsController("res/assets/Sounds/click.ogg");
			_menuSfx[OverBtn] = new SoundsController("res/assets/Sounds/overHim.ogg");

			SetExitKey(KEY_VOLUME_DOWN);
			_tileGrid = new Grid(window);
			_actualLevel = 0;
			_levelCharge = true;
			_levelChoosed = false;
			_onGame = true;
		}
		//---------------------------
		GameHanlde::~GameHanlde()
		{
			if (_tileGrid != NULL)
				delete _tileGrid;
			if (_gameplaySong != NULL)
				delete _gameplaySong;
			if (_menuSong != NULL)
				delete _menuSong;
			if (_playerMove != NULL)
				delete _playerMove;
			for (int i = 0; i < menuSounds; i++) 
			{
				if (_menuSfx[i] != NULL)
					delete _menuSfx[i];
				_menuSfx[i] = NULL;
			}
		}
		void GameHanlde::resetPlayerPos(int tileX, int tileY)
		{
			if (_pj != NULL) {
				_pj->setPosWorld(_tileGrid->tiles[tileX][tileY]->getTilePos());
				_pj->setPosGridX(_tileGrid->tiles[tileX][tileY]->getTileGridX());
				_pj->setPosGridY(_tileGrid->tiles[tileX][tileY]->getTileGridY());
			}
		}
		void GameHanlde::chargeLevel(int whatLevel)
		{
			if (!_levelCharge)
			{
				if (_gameplaySong != NULL)
					_gameplaySong->playSong();

				if (_tileGrid != NULL)
				{
					switch (whatLevel)
					{
					case 0:
						_tileGrid->startLevel_1();
						resetPlayerPos(5, 5);
						break;
					case 1:
						_tileGrid->startLevel_2();
						resetPlayerPos(6, 5);
						break;
					case 2:
						_tileGrid->startLevel_3();
						resetPlayerPos(5, 5);
						break;
					case 3:
						_tileGrid->startLevel_4();
						resetPlayerPos(8, 8);
						break;
					case 4:
						_tileGrid->startLevel_5();
						resetPlayerPos(1, 2);
						break;
					}
				}
				_levelCharge = true;

				if (_UI_Menus[2] != NULL && _UI_Menus[2]->getTypeMenu() == UX_Game) {
					((UX_Menu*)_UI_Menus[2])->setUIActive(true);
				}
			}
			else if (_UI_Menus[2] != NULL && _UI_Menus[2]->getTypeMenu() == UX_Game) {
				if (((UX_Menu*)_UI_Menus[2])->getResetLevel())
				{
					if(_gameplaySong!=NULL)
						_gameplaySong->playSong();

					if (_tileGrid != NULL)
					{
						switch (whatLevel)
						{
						case 0:
							_tileGrid->startLevel_1();
							resetPlayerPos(5, 5);
							break;
						case 1:
							_tileGrid->startLevel_2();
							resetPlayerPos(6, 5);
							break;
						case 2:
							_tileGrid->startLevel_3();
							resetPlayerPos(5, 5);
							break;
						case 3:
							_tileGrid->startLevel_4();
							resetPlayerPos(8, 8);
							break;
						case 4:
							_tileGrid->startLevel_5();
							resetPlayerPos(1, 2);
							break;
						}
					}
					((UX_Menu*)_UI_Menus[2])->resetLevelDone();
				}
				else {
					return;
				}
			}
		}
		//---------------------------
		void GameHanlde::initAll(Screen* window)
		{
			//------------------
			_pj = new Player(window, _tileGrid->tiles[5][5]->getTileGridX(), _tileGrid->tiles[5][5]->getTileGridY(),
				_tileGrid->tiles[5][5]->getTilePos().x, _tileGrid->tiles[5][5]->getTilePos().y);	//INITIAL POS X & Y ON GRID
			//------------------
			_UI_Menus[0] = new MainMenu(window , Start , Credits , Exit);
			//------------------
			_UI_Menus[1] = new LvlMenu(window);
			//------------------
			_UI_Menus[2] = new UX_Menu(window);
			//------------------
			_UI_Menus[3] = new CreditsMenu(window);
			//------------------
			if (_menuSong != NULL && !_menuSong->getIfIsPlaying())
				_menuSong->playSong();
			//------------------
		}
		//---------------------------
		void GameHanlde::updateAll(Screen* window)
		{
			if (_levelChoosed) //---------------------------> GAMEPLAY
			{
				//--------------
				chargeLevel(_actualLevel);
				//--------------
				if (_UI_Menus[2] != NULL && _UI_Menus[2]->getTypeMenu() == UX_Game && !((UX_Menu*)_UI_Menus[2])->getPauseActive())
				{
					//--------------
					if (_gameplaySong != NULL)
						_gameplaySong->updateMusic();
					//--------------
					_directionPj = _pj->getDirection();
					//--------------
					if (_pj != NULL)
					{
						_pj->update(window, _tileGrid->tiles, _tileGrid->getCantMovePlayer());
						if (_pj->wantToMove())
						{
							_playerMove->stopSound();
							_playerMove->playSound();
						}
					}
					//--------------
					if(_tileGrid!=NULL && _pj != NULL)
						_tileGrid->checkBoxMoving(4 ,_pj->getPlayerGridX(),_pj->getPlayerGridY(), _directionPj,
							_pj->wantToMove());
					//--------------	
					if (_tileGrid != NULL)
					{
						if (_tileGrid->playerCanPassToOtherLvL()) 
						{
							if (_UI_Menus[2]!= NULL && _UI_Menus[2]->getTypeMenu() == UX_Game)
							{
								((UX_Menu*)_UI_Menus[2])->setCanPassToOtherLVL(true);

								if (((UX_Menu*)_UI_Menus[2])->canGoToNextLevel() && _actualLevel <= 4)
								{
									_levelCharge = false;
									_actualLevel++;
									((UX_Menu*)_UI_Menus[2])->setCanPassToOtherLVL(false);
									((UX_Menu*)_UI_Menus[2])->setGoToNextLvl(false);
									((UX_Menu*)_UI_Menus[2])->setResetLevelManual(true);
								}
							}
						}
					}
					//--------------	
				}
				if (_UI_Menus[2] != NULL) {

					if (_menuSfx[0] != NULL && _menuSfx[0] != NULL)
						_UI_Menus[2]->inputsMouse(_menuSfx[0], _menuSfx[1]);

					if (_UI_Menus[2]->getTypeMenu() == UX_Game)
					{
						if (((UX_Menu*)_UI_Menus[2])->getWantBackMenu()) {
							_levelChoosed = false;
							((UX_Menu*)_UI_Menus[2])->setWantBack(false);
							if (_UI_Menus[1]->getTypeMenu() == LvlSelector)
								((LvlMenu*)_UI_Menus[1])->setLevelSelectedManual(false);
						}
					}
				}
			}
			else        //---------------------------> MENU INTERFAZ
			{
				//--------------
				if (_gameplaySong != NULL && _gameplaySong->getIfIsPlaying())
				{
					_gameplaySong->stopSong();
					if (_menuSong != NULL)
						_menuSong->playSong();
				}
				//--------------
				if (_UI_Menus[0] != NULL && _UI_Menus[0]->getMenuOn())
				{
					//----------------
					if (_menuSong != NULL)
						_menuSong->updateMusic();
					//----------------

					if (_UI_Menus[2] != NULL && _UI_Menus[2]->getTypeMenu() == UX_Game) {
						((UX_Menu*)_UI_Menus[2])->setPauseActive(false);
						((UX_Menu*)_UI_Menus[2])->setUIActive(false);
					}

					if(_UI_Menus[3]!=NULL && !_UI_Menus[3]->getMenuOn())
					{
						if (_UI_Menus[0] != NULL && _UI_Menus[0]->getTypeMenu() == MenuCore)
						{
							if (!((MainMenu*)_UI_Menus[0])->goLevlSelect())
							{
								if (_menuSfx[0] != NULL && _menuSfx[0] != NULL)
									_UI_Menus[0]->inputsMouse(_menuSfx[0], _menuSfx[1]);

								if (((MainMenu*)_UI_Menus[0])->goCredits())
								{
									((MainMenu*)_UI_Menus[0])->setGoCreditsFalse(false);
									if (_UI_Menus[3]->getTypeMenu() == Creditos)
									{
										((CreditsMenu*)_UI_Menus[3])->setCreditsOn(true);
									}
								}
							}
							else {

								if (_UI_Menus[1] != NULL)
								{
									if (_UI_Menus[1]->getTypeMenu() == LvlSelector)
									{
										if (!_UI_Menus[1]->getMenuOn() && !((LvlMenu*)_UI_Menus[1])->getBackToMenu())
										{
											((LvlMenu*)_UI_Menus[1])->setOnLevlsSelector(true);
										}
										else if (((LvlMenu*)_UI_Menus[1])->getBackToMenu())
										{
											((LvlMenu*)_UI_Menus[1])->setOnLevlsSelector(false);
											((LvlMenu*)_UI_Menus[1])->setBackMenu(false);

											if (_UI_Menus[0]->getTypeMenu() == MenuCore)
											{
												((MainMenu*)_UI_Menus[0])->setBackLvlSel(false);
											}
										}

										if (((LvlMenu*)_UI_Menus[1])->getLevelSelected()) {
											_actualLevel = ((LvlMenu*)_UI_Menus[1])->getLevelToPlay();
											_levelChoosed = true;
											//--------------
											if (_menuSong != NULL && _menuSong->getIfIsPlaying())
												_menuSong->stopSong();
											//--------------
										}
									}
								}
								if(_menuSfx[0]!=NULL && _menuSfx[0] != NULL)
									_UI_Menus[1]->inputsMouse(_menuSfx[0], _menuSfx[1]);
							}
						}

						if (_levelChoosed)
							_levelCharge = false;

						if (_UI_Menus[0]->getTypeMenu() == MenuCore)
						{
							if (((MainMenu*)_UI_Menus[0])->getExitGame() == true)
								_onGame = false;
							else
								_onGame = true;
						}
					}
					else {
						if (_UI_Menus[3] != NULL)
							_UI_Menus[3]->inputsMouse(_menuSfx[0], _menuSfx[1]);
					}
				}
			}
		}
		//---------------------------
		void GameHanlde::drawAll()
		{
			if (_levelChoosed)
			{
				//------------------
				if(_tileGrid!=NULL)
					_tileGrid->drawTilesGrid();
				//------------------
				if(_pj!=NULL)
					_pj->draw();
				//------------------
				if (_UI_Menus[2] != NULL && _UI_Menus[2]->getMenuOn())
					_UI_Menus[2]->draw();
				else if(_UI_Menus[2] != NULL && !_UI_Menus[2]->getMenuOn())
					_UI_Menus[2]->draw();
				//------------------
			}
			else
			{
				if (_UI_Menus[3] != NULL && !_UI_Menus[3]->getMenuOn())
				{
					//------------------
					for (int i = 0; i < 2; i++)
					{
						if (_UI_Menus[i] != NULL)
							_UI_Menus[i]->draw();
					}
					//------------------
				}
				else if (_UI_Menus[3] != NULL && _UI_Menus[3]->getMenuOn()) {
					_UI_Menus[3]->draw();
				}
			}
		}
		//---------------------------
		void GameHanlde::inputs()
		{
			if (_levelChoosed)
			{
				//-------------
				if (_pj != NULL)
					_pj->inputs();
				//-------------
			}
		}
		//---------------------------
		void GameHanlde::deinitAll()
		{
			//-------
			if (_pj != NULL)
				delete _pj;
			//-------
			for (int i = 0; i < cantMaxMenus; i++)
			{
				if (_UI_Menus[i] != NULL)
					delete  _UI_Menus[i];
				_UI_Menus[i] = NULL;
			}
			//-------
			CloseWindow();
			//-------
		}
		//---------------------------
	}
}