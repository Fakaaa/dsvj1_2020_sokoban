#ifndef MENUHANDLE_H
#define MENUHANDLE_H

#include "raylib.h"
#include "Screen/screen.h"
#include "GameHandle/Sfx/Sounds/sounds.h"
using namespace Sokoban;
using namespace ScreenHandle;
using namespace SoundsHandle;

namespace Sokoban {
	namespace MenuHandle {
		//--------------
		enum TYPE_MENU
		{
			MenuCore,
			LvlSelector,
			UX_Game,
			Creditos
		};
		//--------------
		enum TYPE_BTNS
		{
			Start,
			Credits,
			Exit,

			Level_1,
			Level_2,
			Level_3,
			Level_4,
			Level_5,
			Back,

			Resume,
			ToMenu,
			Reset,
			Pause,
			NextLevel
		};
		//--------------
		const int cantMaxMenus = 4;
		//--------------
		class Menu
		{
		private:
			Vector2 _mousePos;
			bool _pressed;
		protected:
			int _pressEfectX;
			int _pressEfectY;
			int _auxScaleX;
			int _auxScaleY;

			TYPE_MENU _type;
		public:
			Menu();
			~Menu();
			Vector2 getMousePos() { return _mousePos; }
			bool getMousePress() { return _pressed; }
			void updateMousePos();
			virtual void inputsMouse(SoundsController* handle1, SoundsController* handle2) = 0;
			virtual void draw()=0;
			virtual bool getMenuOn()=0;
			//virtual bool getLevelSelected()=0;
			virtual int getTypeMenu() = 0;
		};
		//--------------
	}
}
#endif // !MENUHANDLE_H