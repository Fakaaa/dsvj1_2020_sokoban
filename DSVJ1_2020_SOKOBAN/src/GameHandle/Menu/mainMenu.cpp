#include "mainMenu.h"

#include <iostream>

namespace Sokoban {
	namespace MainMenuHanlde {
		//-----------------
		MainMenu::MainMenu(Screen* window, int btn1, int btn2, int btn3) : Menu()
		{
			//-------------------------------
			_type = MenuCore;
			//-------------------------------
			int scaleBgX = 1240;
			int scaleBgY = 720;
			Image resizeBg;

			resizeBg = LoadImage("res/assets/Menu/bgMenu.png");
			ImageResize(&resizeBg, scaleBgX, scaleBgY);
			_bg = LoadTextureFromImage(resizeBg);
			UnloadImage(resizeBg);
			//-------------------------------
			float posX = 0.0f; //
			float posY = 0.0f; //	BUTTONS SETTINGS
			int scaleX = 200;  //
			int scaleY =  65;  //

			_pressEfectX = scaleX;
			_pressEfectY = scaleY;
			//-------------------------------
			if (window != NULL)
			{
				posX = ((window->getScreenWitdh() *0.5f)-(scaleX * 0.5f));
				posY = ((window->getScreenHeigth()*0.5f)-(scaleY * 0.5f));
			}
			//-------------------------------
			for (int i = 0; i < cantBtns; i++)
			{
				_UI[i] = NULL;
			}
			_typeButton = Start;
			int j = 0;
			for (int i = 0; i < cantBtns; i++)
			{
				_UI[i] = new Button(posX, (posY + (120*i)), scaleX, scaleY,_typeButton+j);
				j++;
			}
			//-------------------------------
			_onMainMenu = true;
			_onExitGame = false; 
			_goLevlSelect = false;
			_goCredits = false;
			_delayToStar = 0.0f;
			_timeToBeign = 0.4f;
			_wantStar = false;
			//-------------------------------
			setMainMenuButtons(btn1, btn2, btn3);
			//-------------------------------
			_auxScaleX = scaleX;
			_auxScaleY = scaleY;
		}
		//-----------------
		MainMenu::~MainMenu()
		{
			for (int i = 0; i < cantBtns; i++)
			{
				if (_UI[i] != NULL)
					delete _UI[i];
				_UI[i] = NULL;
			}
			UnloadTexture(_bg);
		}
		//-----------------
		void MainMenu::setMainMenuButtons(int first, int second, int thidr)
		{
			if (_UI[first]!=NULL)
				_UI[first]->setType(Start);
			if (_UI[second]!= NULL)
				_UI[second]->setType(Credits);
			if (_UI[thidr]!= NULL)
				_UI[thidr]->setType(Exit);
		}
		bool MainMenu::goLevlSelect()
		{
			if (_goLevlSelect)
				return true;
			else
				return false;
		}
		bool MainMenu::goCredits()
		{
			if (_goCredits)
				return true;
			else
				return false;
		}
		void MainMenu::setOnMainMenu(bool what)
		{
			_onMainMenu = what;
		}
		void MainMenu::setBackLvlSel(bool what)
		{
			_goLevlSelect = what;
		}
		void MainMenu::setGoCreditsFalse(bool hehe)
		{
			_goCredits = hehe;
		}
		//-----------------
		void MainMenu::inputsMouse(SoundsController* handle1, SoundsController* handle2)
		{
			//--------------------
			//if (_wantStar && _delayToStar <= _timeToBeign)
			//	_delayToStar += GetFrameTime();
			//if (_delayToStar >= _timeToBeign)
			//{
			//	_onMainMenu = false;
			//	_delayToStar = 0;
			//}

			//--------------------
			if (_UI[Start] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[Start]->getCollider()))
				{
					_UI[Start]->setScaleX(_pressEfectX + 20);
					_UI[Start]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[Start]->setColorText(YELLOW);
						//_onMainMenu = false;
						_goLevlSelect = true;
						//_wantStar = true;
					}
				}
				else {
					_UI[Start]->setScaleX(_auxScaleX);
					_UI[Start]->setScaleY(_auxScaleY);
					_UI[Start]->setColorText(WHITE);
				}
			}
			//--------------------
			if (_UI[Credits] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[Credits]->getCollider()))
				{
					_UI[Credits]->setScaleX(_pressEfectX + 20);
					_UI[Credits]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress()) {
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_goCredits = true;
						_UI[Credits]->setColorText(YELLOW);
					}
				}
				else {
					_UI[Credits]->setScaleX(_auxScaleX);
					_UI[Credits]->setScaleY(_auxScaleY);
					_UI[Credits]->setColorText(WHITE);
				}
			}
			//--------------------
			if (_UI[Exit] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[Exit]->getCollider()))
				{
					_UI[Exit]->setScaleX(_pressEfectX + 20);
					_UI[Exit]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[Exit]->setColorText(YELLOW);
						_onExitGame = true;
					}
				}
				else {
					_UI[Exit]->setScaleX(_auxScaleX);
					_UI[Exit]->setScaleY(_auxScaleY);
					_UI[Exit]->setColorText(WHITE);
				}
			}
			//--------------------
			Menu::updateMousePos();
			//--------------------
		}
		//-----------------
		void MainMenu::draw()
		{
			//-----------
			DrawTexture(_bg, 0, 0, WHITE);
			//-----------
			if (!_goLevlSelect)
			{
				for (int i = 0; i < cantBtns; i++)
				{
					if (_UI[i] != NULL)
					{
						DrawTextureEx(_UI[i]->getBtn(), _UI[i]->getPos(), 0.0f, 1.0f, LIGHTGRAY);
						DrawTextRec(_UI[i]->getFontTextBtn(), _UI[i]->getTextBtn(), _UI[i]->getCollider(), _UI[i]->getFontSize(),
							_UI[i]->getSpacing(), _UI[i]->getWordlWrap(), _UI[i]->getColorText());
						#if DEBUG
						DrawRectangleLinesEx(_UI[i]->getCollider(), 1, GREEN);
						#endif
					}
				}
			}
			//-----------
		}
		bool MainMenu::getMenuOn()
		{
			if (_onMainMenu)
				return true;
			else
				return false;
		}
		bool MainMenu::getExitGame()
		{
			if (_onExitGame)
				return true;
			else
				return false;
		}
		//-----------------
	}
}