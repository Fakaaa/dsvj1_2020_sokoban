﻿#include "buttons.h"

namespace Sokoban {
	namespace ButtonsHandle {
		//-----------------
		Button::Button(float posX, float posY, int scaleX, int scaleY,int type)
		{
			_posButton = { posX, posY };
			_scaleX = scaleX;
			_scaleY = scaleY;
			_collider = { _posButton.x+20, _posButton.y+10, (float)(_scaleX*0.8f), (float)(_scaleY*0.8f) };
			
			_textBtn.text = " ";
			_textBtn.spacing = 8.0f;
			_textBtn.worldWrap = false;
			_textBtn.fontSize = 50;
			_textBtn.colorText = WHITE;

			loadFont();

			_selected = false;
			_type = type;
			switch (_type)
			{
			case 0:
				setTextButton("  Start");
				break;
			case 1:
				setTextButton(" Credits");
				break;
			case 2:
				setTextButton("   Exit");
				break;
			case 3:
				setTextButton("  1 ");
				break;			 
			case 4:				 
				setTextButton("  2 ");
				break;			 
			case 5:				 
				setTextButton("  3 ");
				break;			 
			case 6:				 
				setTextButton("  4 ");
				break;			 
			case 7:				 
				setTextButton("  5 ");
				break;
			case 8:
				setTextButton(" <- ");
				break;
			case 9:
				setTextButton("Resume");
				break;
			case 10:
				setTextButton("ToMenu");
				break;
			case 11:
				setTextButton(" Reset");
				break;
			case 12:
				setTextButton("   | | ");
				break;
			case 13:
				setTextButton("Nextlvl");
				break;
			}
			loadTextures(_scaleX, _scaleY);
		}
		//-----------------
		Button::~Button()
		{
			unloadTextures();
		}
		void Button::loadFont()
		{
			_textBtn.font = LoadFont("res/assets/Font/DUNGRG__.TTF");
		}
		void Button::setScaleX(int scaleX)
		{
			_scaleX = scaleX;
			_texture.width = _scaleX;
			_collider.width = _scaleX;
			_textBtn.fontSize = (int)(_scaleX / 5);
		}
		void Button::setScaleY(int scaleY)
		{
			_scaleY = scaleY;
			_texture.height = _scaleY;
			_collider.height = _scaleY;
		}
		//-----------------
		void Button::setColorText(Color colorcin)
		{
			_textBtn.colorText = colorcin;
		}
		//-----------------
		void Button::setFontSize(int size)
		{
			_textBtn.fontSize = size;
		}
		//-----------------
		void Button::setTextButton(const char * textHere)
		{
			_textBtn.text = textHere;
		}
		//-----------------
		void Button::setBtnScale(int scaleX, int scaleY)
		{
			_scaleX = scaleX;
			_scaleY = scaleY;
		}
		//-----------------
		void Button::setType(int type)
		{
			_type = type;
		}
		//-----------------
		void Button::loadTextures(int& scaleX, int& scaleY)
		{
			Image resize;
			resize = LoadImage("res/assets/Buttons/buttonRed.png");
			ImageResize(&resize, scaleX, scaleY);
			_texture = LoadTextureFromImage(resize);
			UnloadImage(resize);
		}
		//-----------------
		void Button::unloadTextures()
		{
			UnloadTexture(_texture);
		}
		//-----------------
	}
}