#ifndef LVLMENU_H
#define LVLMENU_H

#include "GameHandle/Menu/menuHandle.h"
#include "GameHandle/Menu/Buttons/buttons.h"
using namespace Sokoban;
using namespace MenuHandle;
using namespace ButtonsHandle;

namespace Sokoban {
	namespace LevelHanlde {
		//---------------
		const int cantBtns = 6;
		//---------------
		class LvlMenu : public Menu
		{
		private:
			bool _onLvlSelection;
			bool _BackMenu;
			bool _levelSelected;
			Button* _UI[cantBtns];

			int _whatLevel;
		public:
			TYPE_BTNS _typeButton;

			LvlMenu(Screen* window);
			~LvlMenu();
			void setOnLevlsSelector(bool on);
			void setLevelsButtons(int firstLvl, int secondLvl, int thirdLvl,
				int fourthLvl, int fiveLvl);
			void setBackMenu(bool what);
			void selectLeveleToPlay(int whatLevel);
			int getLevelToPlay() { return  _whatLevel; }
			void setLevelSelectedManual(bool what);
			bool getLevelSelected();

			bool getBackToMenu();
			virtual void inputsMouse(SoundsController* handle1, SoundsController* handle2);
			virtual void draw();
			virtual bool getMenuOn();
			virtual int getTypeMenu() { return _type; }
		};
		//---------------
	}
}
#endif // !LVLMENU_H