#include "lvlMenu.h"

namespace Sokoban {
	namespace LevelHanlde {
		//----------------
		LvlMenu::LvlMenu(Screen* window)
		{
			float posX = 0.0f; //
			float posY = 0.0f; //	BUTTONS SETTINGS
			int scaleX = 115;  //
			int scaleY = 65;  //

			_type = LvlSelector;
			_onLvlSelection = false;
			_BackMenu=false;
			_levelSelected = false;
			_pressEfectX = scaleX;
			_pressEfectY = scaleY;
			_whatLevel = 0;

			if (window != NULL)
			{
				posX = ((window->getScreenWitdh() *0.2f) - (scaleX * 0.5f));
				posY = ((window->getScreenHeigth()*0.6f) - (scaleY * 0.5f));
			}
			for (int i = 0; i < cantBtns; i++) { _UI[i] = NULL;}
			_typeButton = Level_1;
			int j = 0;
			for (int i = 0; i < cantBtns; i++)
			{
				if(i < 5)
					_UI[i] = new Button((posX + (200*i)), posY, scaleX, scaleY, _typeButton+j);
				else
					_UI[i] = new Button( posX , posY + 200, scaleX, scaleY, _typeButton + j);
				j++;
			}
			setLevelsButtons(Level_1, Level_2, Level_3, Level_4, Level_5);
			
			_auxScaleX = scaleX;
			_auxScaleY = scaleY;
		}
		//----------------
		LvlMenu::~LvlMenu()
		{
			for (int i = 0; i < cantBtns; i++)
			{
				if (_UI[i] != NULL)
					delete _UI[i];
				_UI[i] = NULL;
			}
		}
		void LvlMenu::setOnLevlsSelector(bool on)
		{
			_onLvlSelection = on;
		}
		void LvlMenu::setLevelsButtons(int firstLvl, int secondLvl, int thirdLvl, int fourthLvl, int fiveLvl)
		{
			if (_UI[0] != NULL)
				_UI[0]->setType(Level_1);
			if (_UI[1] != NULL)
				_UI[1]->setType(Level_2);
			if (_UI[2] != NULL)
				_UI[2]->setType(Level_3);
			if (_UI[3] != NULL)
				_UI[3]->setType(Level_4);
			if (_UI[4] != NULL)
				_UI[4]->setType(Level_5);
			if (_UI[5] != NULL)
				_UI[5]->setType(Back);
		}
		void LvlMenu::setBackMenu(bool what)
		{
			_BackMenu = what;
		}
		void LvlMenu::selectLeveleToPlay(int whatLevel)
		{
			switch (whatLevel)
			{
			case 0:
				_levelSelected = true;
				_whatLevel = 0;
				break;
			case 1:
				_levelSelected = true;
				_whatLevel = 1;
				break;
			case 2:
				_levelSelected = true;
				_whatLevel = 2;
				break;
			case 3:
				_levelSelected = true;
				_whatLevel = 3;
				break;
			case 4:
				_levelSelected = true;
				_whatLevel = 4;
				break;
			}
		}
		bool LvlMenu::getBackToMenu()
		{
			if (_BackMenu)
				return true;
			else
				return false;
		}
		void LvlMenu::inputsMouse(SoundsController* handle1, SoundsController* handle2)
		{
			if (_UI[0] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[0]->getCollider())) {
					_UI[0]->setScaleX(_pressEfectX + 20);
					_UI[0]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						selectLeveleToPlay(0);
					}
				}
				else {
					_UI[0]->setScaleX(_auxScaleX);
					_UI[0]->setScaleY(_auxScaleY);
					_UI[0]->setColorText(WHITE);
				}
			}
			if (_UI[1] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[1]->getCollider())) {
					_UI[1]->setScaleX(_pressEfectX + 20);
					_UI[1]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						selectLeveleToPlay(1);
					}
				}
				else {
					_UI[1]->setScaleX(_auxScaleX);
					_UI[1]->setScaleY(_auxScaleY);
					_UI[1]->setColorText(WHITE);
				}
			}
			if (_UI[2] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[2]->getCollider())) {
					_UI[2]->setScaleX(_pressEfectX + 20);
					_UI[2]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						selectLeveleToPlay(2);
					}
				}
				else {
					_UI[2]->setScaleX(_auxScaleX);
					_UI[2]->setScaleY(_auxScaleY);
					_UI[2]->setColorText(WHITE);
				}
			}
			if (_UI[3] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[3]->getCollider())) {
					_UI[3]->setScaleX(_pressEfectX + 20);
					_UI[3]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						selectLeveleToPlay(3);
					}
				}
				else {
					_UI[3]->setScaleX(_auxScaleX);
					_UI[3]->setScaleY(_auxScaleY);
					_UI[3]->setColorText(WHITE);
				}
			}
			if (_UI[4] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[4]->getCollider())) {
					_UI[4]->setScaleX(_pressEfectX + 20);
					_UI[4]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						selectLeveleToPlay(4);
					}
				}
				else {
					_UI[4]->setScaleX(_auxScaleX);
					_UI[4]->setScaleY(_auxScaleY);
					_UI[4]->setColorText(WHITE);
				}
			}
			if (_UI[5] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[5]->getCollider())) {
					_UI[5]->setScaleX(_pressEfectX + 20);
					_UI[5]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress()) 
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[5]->setColorText(YELLOW);
						_BackMenu = true;
						_onLvlSelection = false;
					}
				}
				else {
					_UI[5]->setScaleX(_auxScaleX);
					_UI[5]->setScaleY(_auxScaleY);
					_UI[5]->setColorText(WHITE);
				}
			}
			//--------------------
			Menu::updateMousePos();
			//--------------------
		}
		void LvlMenu::draw()
		{
			//-----------
			if (_onLvlSelection)
			{
				for (int i = 0; i < cantBtns; i++)
				{
					if (_UI[i] != NULL)
					{
						DrawTextureEx(_UI[i]->getBtn(), _UI[i]->getPos(), 0.0f, 1.0f, LIGHTGRAY);
						DrawTextRec(_UI[i]->getFontTextBtn(), _UI[i]->getTextBtn(), _UI[i]->getCollider(), _UI[i]->getFontSize(),
							_UI[i]->getSpacing(), _UI[i]->getWordlWrap(), _UI[i]->getColorText());
						#if DEBUG
						DrawRectangleLinesEx(_UI[i]->getCollider(), 1, GREEN);
						#endif
					}
				}
			}
			//-----------
		}
		bool LvlMenu::getMenuOn()
		{
			if (_onLvlSelection)
				return true;
			else
				return false;
		}
		void LvlMenu::setLevelSelectedManual(bool what)
		{
			_levelSelected = what;
		}
		bool LvlMenu::getLevelSelected()
		{
			if (_levelSelected)
				return true;
			else
				return false;
		}
		//----------------
	}
}