#include "credits.h"

namespace Sokoban {
	namespace CreditsHandlde {
		//-------------

		void CreditsMenu::loadTexture()
		{
			_credits = LoadTexture("res/assets/Menu/credits.png");
		}

		void CreditsMenu::unloadTexture()
		{
			UnloadTexture(_credits);
		}

		CreditsMenu::CreditsMenu(Screen * window)
		{
			float posX = 0.0f; //
			float posY = 0.0f; //	BUTTONS SETTINGS
			int scaleX = 115;  //
			int scaleY = 65;  //

			_type = Creditos;
			_typeButton = Back;
			_creditsOn = false;

			loadTexture();

			if (window != NULL)
			{
				posX = ((window->getScreenWitdh() * 0.1f) - (scaleX * 0.5f));
				posY = ((window->getScreenHeigth()*0.8f) - (scaleY * 0.5f));
			}
			_UI = NULL;
			_UI = new Button(posX, posY, scaleX, scaleY, _typeButton);

			_pressEfectX = scaleX;
			_pressEfectY = scaleY;
			_auxScaleX = scaleX;
			_auxScaleY = scaleY;
		}

		CreditsMenu::~CreditsMenu()
		{
			if (_UI != NULL)
				delete _UI;
			_UI = NULL;

			unloadTexture();
		}

		void CreditsMenu::setCreditsOn(bool what)
		{
			_creditsOn = what;
		}

		void CreditsMenu::inputsMouse(SoundsController * handle1, SoundsController * handle2)
		{
			//-------------
			if (_UI != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI->getCollider())) {
					_UI->setScaleX(_pressEfectX + 20);
					_UI->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI->setColorText(YELLOW);
						_creditsOn = false;
					}
				}
				else {
					_UI->setScaleX(_auxScaleX);
					_UI->setScaleY(_auxScaleY);
					_UI->setColorText(WHITE);
				}
			}
			//--------------------
			Menu::updateMousePos();
			//--------------------
		}

		void CreditsMenu::draw()
		{
			//---------------
			DrawTexture(_credits, 0, 0, WHITE);
			//---------------
			if (_UI != NULL)
			{
				DrawTextureEx(_UI->getBtn(), _UI->getPos(), 0.0f, 1.0f, LIGHTGRAY);
				DrawTextRec(_UI->getFontTextBtn(), _UI->getTextBtn(), _UI->getCollider(), _UI->getFontSize(),
					_UI->getSpacing(), _UI->getWordlWrap(), _UI->getColorText());
				#if DEBUG
				DrawRectangleLinesEx(_UI->getCollider(), 1, GREEN);
				#endif
			}
				
		}

		bool CreditsMenu::getMenuOn()
		{
			if (_creditsOn)
				return true;
			else
				return false;
		}

		//-------------
	}
}