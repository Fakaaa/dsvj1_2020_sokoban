#ifndef CREDITS_H
#define CREDITS_H

#include "GameHandle/Menu/menuHandle.h"
#include "GameHandle/Menu/Buttons/buttons.h"
using namespace Sokoban;
using namespace MenuHandle;
using namespace ButtonsHandle;

namespace Sokoban {
	namespace CreditsHandlde {
		//--------------
		class CreditsMenu : public Menu
		{
		private:
			bool _creditsOn;
			Button* _UI;
			Texture2D _credits;

			TYPE_MENU _type;
		protected:
			void loadTexture();
			void unloadTexture();
		public:
			TYPE_BTNS _typeButton;

			CreditsMenu(Screen* window);
			~CreditsMenu();
			void setMainMenuButtons(int first, int second, int thidr);
			void setCreditsOn(bool what);
			virtual void inputsMouse(SoundsController* handle1, SoundsController* handle2);
			virtual void draw();
			virtual bool getMenuOn();
			virtual int getTypeMenu() { return _type; }
		};
		//--------------
	}
}
#endif // !CREDITS_H