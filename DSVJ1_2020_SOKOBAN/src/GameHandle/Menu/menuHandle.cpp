#include "menuHandle.h"

namespace Sokoban {
	namespace MenuHandle {
		//----------
		Menu::Menu()
		{
			_mousePos = GetMousePosition();
			_pressed = false;
		}
		//----------
		Menu::~Menu()
		{
		}
		void Menu::updateMousePos()
		{
			_mousePos = GetMousePosition();
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) { _pressed = true; }
			else { _pressed = false; }
			#if DEBUG
			DrawCircleV(_mousePos, 4, GREEN);
			#endif
		}
		//----------
	}
}