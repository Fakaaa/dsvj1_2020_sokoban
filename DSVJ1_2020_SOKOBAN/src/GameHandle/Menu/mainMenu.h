#ifndef MAINMENU_H
#define MAINMENU_H

#include "menuHandle.h"
#include "Buttons/buttons.h"
using namespace Sokoban;
using namespace MenuHandle;
using namespace ButtonsHandle;

namespace Sokoban {
	namespace MainMenuHanlde {
		//--------------
		const int cantBtns = 3;
		//--------------
		class MainMenu : public Menu
		{
		private:
			bool _onMainMenu;
			bool _goLevlSelect;
			bool _goCredits;
			bool _onExitGame;
			Button* _UI[cantBtns];
			Texture2D _bg;

			float _delayToStar;
			float _timeToBeign;
			bool _wantStar;
		public:
			TYPE_BTNS _typeButton;

			MainMenu(Screen* window, int btn1, int btn2, int btn3);
			~MainMenu();
			void setMainMenuButtons(int first, int second, int thidr);
			bool getExitGame();
			bool goLevlSelect();
			bool goCredits();
			void setOnMainMenu(bool what);
			void setBackLvlSel(bool what);
			void setGoCreditsFalse(bool hehe);
			virtual void inputsMouse(SoundsController* handle1, SoundsController* handle2);
			virtual void draw();
			virtual bool getMenuOn();
			virtual int getTypeMenu() { return _type; }
		};
		//--------------
	}
}
#endif // !MAINMENU_H