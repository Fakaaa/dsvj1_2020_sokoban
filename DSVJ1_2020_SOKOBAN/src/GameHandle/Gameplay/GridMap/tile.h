#ifndef TILE_H
#define TILE_H

#include "raylib.h"

namespace Sokoban {
	namespace TileHandle {
		//------------
		enum TYPE_TILE
		{
			Floor,
			Wall,
			Box,
			Point,
			Empty
		};
		enum SPECIFICATION
		{
			Top,
			Left,
			Rigth,
			Bot
		};
		//------------------------------
		class Tile
		{
		private:
			int _gridPosX;
			int _gridPosY;
			//---
			Vector2 _tilePos;
			float _tileScaleX;
			float _tileScaleY;
			//---
			TYPE_TILE _type;
			SPECIFICATION _orientation;
			//---
			Rectangle _tileBoxCol;
		public:
			Tile(float x, float y, float w, float h, TYPE_TILE type, int gridPosX, int gridPosY);
			Tile(TYPE_TILE type);
			~Tile();
			//-------------------------------
			int getTileGridX() { return _gridPosX; }
			int getTileGridY() { return _gridPosY; }
			Vector2 getTilePos() { return _tilePos; }
			Rectangle getCollider() { return _tileBoxCol; }
			void setPosTile(float x, float y);
			void setPosGridTileX(int x);
			void setPosGridTileY(int y);
			//-------------------------------
			float getScaleX() { return _tileScaleX; }
			float getScaleY() { return _tileScaleY; }
			//-------------------------------
			void setTypeTile(TYPE_TILE type);
			TYPE_TILE getTypeTile() { return _type; }
			//-------------------------------
			SPECIFICATION getOrientation() { return _orientation; }
			void setOrientation(SPECIFICATION direction);
			//-------------------------------
			void drawColliderTile();
		};
		//------------
	}
}
#endif // !TILE_H