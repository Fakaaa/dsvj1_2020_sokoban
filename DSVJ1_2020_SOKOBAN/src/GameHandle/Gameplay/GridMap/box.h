#ifndef BOX_H
#define BOX_H

#include "raylib.h"
#include "GameHandle/Gameplay/GridMap/tile.h"
using namespace Sokoban;
using namespace TileHandle;

namespace Sokoban {
	namespace BoxHandle {
		//----------------
		class BoxTile : public Tile
		{
		private:
			bool _isInteractable;

			bool _imInPlace;

			bool _onSlide;
			bool _sheMovesTop;
			bool _sheMovesBot;
			bool _sheMovesLeft;
			bool _sheMovesRigth;

			float _toMoveWorldX;
			float _toMoveWorldY;
		public:
			int toMoveX;
			int toMoveY;
			int _gridPosXBox;
			int _gridPosYBox;

			//------------------
			BoxTile(bool is,float x, float y, float w, float h, TYPE_TILE type, int gridPosX, int gridPosY);
			BoxTile(TYPE_TILE type);
			~BoxTile();
			//------------------
			void setInPlacePoint(bool placed);
			bool getIfIsPlaced() { return _imInPlace; }
			void setInteractable(bool what);
			bool getInteractable() { return _isInteractable; }
			bool getOnSlide() { return _onSlide; }
			void setOnSlide(bool itIs);
			//------------------
			void setBOXGRIDPosX(int xpos);
			int getBOXGridX() { return _gridPosXBox; }
			void setBOXGRIDPosY(int ypos);
			int getBOXGridY() { return _gridPosYBox; }
			void setBOXWorldPosX(float xpos);
			float getBOXWorldX() { return _toMoveWorldX; }
			void setBOXWorldPosY(float ypos);
			float getBOXWorldY() { return _toMoveWorldY; }

			void setToMoveX(int x);
			void setToMoveY(int y);
			//------------------
			void setPosTileMap(Tile* tileGrid[10][10], int& playerDirecetion);
			void placBoxOnTile(float& newPosx, float& newPosy);
			//------------------
		};
		//----------------
	}
}
#endif // !BOX_H