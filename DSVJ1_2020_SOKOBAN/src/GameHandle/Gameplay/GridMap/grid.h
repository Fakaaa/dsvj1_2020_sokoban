#ifndef GRID_H
#define  GRID_H

#include "tile.h"
#include "box.h"
#include "Screen/screen.h"
using namespace Sokoban;
using namespace TileHandle;
using namespace BoxHandle;
using namespace ScreenHandle;

namespace Sokoban {
	namespace GridHandle {
		//------------------------------
		const int gridX = 10;
		const int gridY = 10;
		const int tilesTexture = 10;
		const int _maxBoxesForLevel = 4;
		const int canLevels = 5;
		//------------------------------
		enum LEVEL_LOADED
		{
			Level_1,
			Level_2,
			Level_3,
			Level_4,
			Level_5
		};
		//------------------------------
		class Grid
		{
		private:
			Texture2D _whatTile[tilesTexture];
			LEVEL_LOADED _previusLevelLoaded;
			LEVEL_LOADED _actualLevelLoaded;
			//-----------
			struct BOX_PARAMETERS
			{
				float WORLD_POSX;
				float WORLD_POSY;
				int TILE_POSX;
				int TILE_POSY;
			};
			//-----------
			bool _boxesAlreadyCreate;
			//-----------
		public:
			//-----------
			int _boxesToPassLvl;
			int _actualBoxesPlaced;
			//-----------
			float BOXES_SCALE;
			//-----------
			BOX_PARAMETERS b1[canLevels];
			BOX_PARAMETERS b2[canLevels];
			BOX_PARAMETERS b3[canLevels];
			BOX_PARAMETERS b4[canLevels];
			//-----------
		public:
			Tile* tiles[gridX][gridY];
			BoxTile* boxes[_maxBoxesForLevel];
			Grid(Screen* w);
			~Grid();
			//----------
			void cleanTileMap();
			void placeBasicWalls();
			void placeCustomWalls(int gridX, int gridY, SPECIFICATION orientation);
			void placeCustomTile(int gridX, int gridY, TYPE_TILE type);
			void startLevel_1();
			void startLevel_2();
			void startLevel_3();
			void startLevel_4();
			void startLevel_5();

			bool playerCanPassToOtherLvL();
			//----------
			void setBoxesSttngPerLvl(LEVEL_LOADED whatLevl,int box1ToGridX,int box1ToGridY, int box2ToGridX, int box2ToGridY,
				int box3ToGridX,int box3ToGridY, int box4ToGridX, int box4ToGridY);
			void destroyLevlBoxes();
			void createBoxesPerLevel(LEVEL_LOADED whatLvl);
			//----------
			void checkBoxMoving(int cantBoxesActive,int xGrid, int yGrid,int& playerDirection, bool isOnTranslate);
			void boxesMovement(int actualBox, int otherBox1, int otherBox2, int otherBox3
				,int xGrid, int yGrid, int playerDirection, bool isOnTranslate);
			//----------
			void rotateTextureAndDraw(Texture2D& toRote, int X, int Y);
			void drawTilesGrid();
			//----------
			void loadTiles(float wTile, float hTile);
			void unloadTiles();
			//----------
			bool getCantMovePlayer();
		};
		//------------------------------
	}
}
#endif // !GRID_H