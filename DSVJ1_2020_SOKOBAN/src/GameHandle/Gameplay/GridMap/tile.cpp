#include "tile.h"

namespace Sokoban {
	namespace TileHandle {
		//-----------------
		Tile::Tile(float x, float y, float w, float h, TYPE_TILE type, int gridPosX, int gridPosY)
		{
			//-------------
			_tilePos = {x , y};
			_tileScaleX = w;
			_tileScaleY = h;
			//-------------
			_type = type;
			//-------------
			_gridPosX = gridPosX;
			_gridPosY = gridPosY;
			//-------------
			_tileBoxCol = { _tilePos.x, _tilePos.y, _tileScaleX, _tileScaleY };
		}
		Tile::Tile(TYPE_TILE type)
		{
			_type = type;
		}
		//-----------------
		Tile::~Tile(){ /* Xd lol*/ }
		//-----------------
		void Tile::setPosTile(float x, float y)
		{
			_tilePos.x = x;
			_tilePos.y = y;
		}
		//-----------------
		void Tile::setPosGridTileX(int x)
		{
			_gridPosX = x;
		}
		//-----------------
		void Tile::setPosGridTileY(int y)
		{
			_gridPosY = y;
		}
		//-----------------
		void Tile::setTypeTile(TYPE_TILE type)
		{
			_type = type;
		}
		//-----------------
		void Tile::setOrientation(SPECIFICATION direction)
		{
			_orientation = direction;
		}
		//-----------------
		void Tile::drawColliderTile()
		{
			//---------
			DrawRectangleLinesEx(_tileBoxCol, 1, GREEN);
			//---------
		}
		//-----------------
	}
}