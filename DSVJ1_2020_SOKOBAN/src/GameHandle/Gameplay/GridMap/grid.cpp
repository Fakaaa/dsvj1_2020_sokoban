#include <iostream>

#include "grid.h"

#include "GameHandle/Gameplay/Player/player.h"
using namespace PlayerHandle;

namespace Sokoban {
	namespace GridHandle {
		void Grid::cleanTileMap()
		{
			for (int i = 0; i < gridY; i++)
			{
				for (int j = 0; j < gridX; j++)
				{
					if (tiles[j][i] != NULL)
						tiles[j][i]->setTypeTile(TYPE_TILE::Floor);
				}
			}
		}
		//-----------------------------
		void Grid::placeBasicWalls()
		{
			//========================================
			for (int i = 0; i < gridX; i++)
			{
				if (tiles[i][0] != NULL)
				{
					tiles[i][0]->setTypeTile(TYPE_TILE::Wall);
					tiles[i][0]->setOrientation(SPECIFICATION::Top);
				}
			}
			for (int i = 0; i < gridX; i++)
			{
				if (tiles[i][gridY - 1] != NULL)
				{
					tiles[i][gridY - 1]->setTypeTile(TYPE_TILE::Wall);
					tiles[i][gridY - 1]->setOrientation(SPECIFICATION::Bot);
				}
			}
			for (int i = 0; i < gridY; i++)
			{
				if (tiles[0][i] != NULL)
				{
					tiles[0][i]->setTypeTile(TYPE_TILE::Wall);
					if (i == gridY - 1)
						tiles[0][i]->setOrientation(SPECIFICATION::Top);
					else
						tiles[0][i]->setOrientation(SPECIFICATION::Rigth);
				}
			}
			for (int i = 0; i < gridY; i++)
			{
				if (tiles[gridX - 1][i] != NULL)
				{
					tiles[gridX - 1][i]->setTypeTile(TYPE_TILE::Wall);
					if (i == gridY - 1)
						tiles[gridX - 1][i]->setOrientation(SPECIFICATION::Top);
					else
						tiles[gridX - 1][i]->setOrientation(SPECIFICATION::Rigth);
				}
			}
			//========================================
		}
		void Grid::placeCustomWalls(int gridX, int gridY, SPECIFICATION orientation)
		{
			if (tiles[gridX][gridY] != NULL)
			{
				tiles[gridX][gridY]->setTypeTile(TYPE_TILE::Wall);
				tiles[gridX][gridY]->setOrientation(orientation);
			}
		}
		void Grid::placeCustomTile(int gridX, int gridY, TYPE_TILE type)
		{
			if (tiles[gridX][gridY] != NULL)
				tiles[gridX][gridY]->setTypeTile(type);
		}
		//----------------
		void Grid::startLevel_1()
		{
			//---------------------	CLEAN MAP
			cleanTileMap();
			//---------------------	BOXES SETTINGS
			setBoxesSttngPerLvl(Level_1, 2, 3, 6, 5, 6, 6, 8, 7);
			//---------------------	WALLS
			placeBasicWalls();
			//-------------------------
			for (int i = 0; i < gridY / 2; i++)
			{
				if (tiles[gridX / 2][i] != NULL)
				{
					tiles[gridX/2][i]->setTypeTile(TYPE_TILE::Wall);

					if(i == ((gridY/2)-1))
						tiles[gridX/2][i]->setOrientation(SPECIFICATION::Top);
					else
						tiles[gridX/2][i]->setOrientation(SPECIFICATION::Rigth);
				}
			}
			//---------------------------
			for (int i = 0; i < 3; i++)
			{
				if (tiles[3][i] != NULL)
				{
					tiles[3][i]->setTypeTile(TYPE_TILE::Wall);

					if (i == (3 - 1))
						tiles[3][i]->setOrientation(SPECIFICATION::Top);
					else
						tiles[3][i]->setOrientation(SPECIFICATION::Rigth);
				}
			}
			//---------------------------
			for (int i = gridY-1; i > 6; i--)
			{
				if (tiles[6][i] != NULL)
				{
					tiles[6][i]->setTypeTile(TYPE_TILE::Wall);

					if (i == (gridY - 1))
						tiles[6][i]->setOrientation(SPECIFICATION::Top);
					else
						tiles[6][i]->setOrientation(SPECIFICATION::Rigth);
				}
			}
			//--------------- POINTS WHERE PLACE BOXES
			placeCustomTile(4, 1, Point);
			placeCustomTile(6, 1, Point);
			placeCustomTile(7, 1, Point);
			placeCustomTile(8, 1, Point);
			//--------------- BOXEs
			placeCustomTile(2, 3, Box);
			placeCustomTile(6, 5, Box);
			placeCustomTile(6, 6, Box);
			placeCustomTile(8, 7, Box);
			//---------------------------------
			_boxesToPassLvl = 4;
			_actualBoxesPlaced = 0;
			//---------------------------------
			if (!_boxesAlreadyCreate)
			{
				createBoxesPerLevel(Level_1);
				_boxesAlreadyCreate = true;
			}
			else {
				destroyLevlBoxes();
				createBoxesPerLevel(Level_1);
			}
			//---------------------------------
			_actualLevelLoaded = Level_1;
			//---------------------------------
		}
		//----------------
		void Grid::startLevel_2()
		{
			//------------------------	CLEAN MAP
			cleanTileMap();
			//------------------------
			setBoxesSttngPerLvl(Level_2, 4, 7, 5, 7, 6, 7, 7, 7);
			//------------------------
			placeBasicWalls();
			//------------------------	//MORE WALLS
			for (int i = 0; i < gridY / 2; i++)
			{
				if (tiles[i][gridY / 2] != NULL)
				{
					tiles[i][gridY / 2]->setTypeTile(TYPE_TILE::Wall);

					if (i == 0 || i == 3)
						tiles[i][gridY / 2]->setOrientation(SPECIFICATION::Rigth);
					else
						tiles[i][gridY / 2]->setOrientation(SPECIFICATION::Top);
				}
			}
			//------------------------
			for (int i = gridX-1; i > 2; i--)
			{
				if (tiles[i][3] != NULL)
				{
					if(i != 6 && i!= 5 && i!= 8)
						tiles[i][3]->setTypeTile(TYPE_TILE::Wall);
					else
						tiles[i][3]->setTypeTile(TYPE_TILE::Floor);

					if (i == gridX-1 || i == 3)
						tiles[i][3]->setOrientation(SPECIFICATION::Rigth);
					else
						tiles[i][3]->setOrientation(SPECIFICATION::Top);
				}
			}
			//------------------------
			placeCustomWalls(3, 8, Rigth);
			placeCustomWalls(3, 6, Top);
			placeCustomWalls(3, 4, Rigth);
			//------------------------
			//--------------- POINTS WHERE PLACE BOXES

			placeCustomTile(1, 4, Point);
			placeCustomTile(1, 6, Point);
			placeCustomTile(8, 1, Point);
			placeCustomTile(8, 2, Point);
			//------------------------
			//--------------- BOXEs
			placeCustomTile(4, 7, Box);
			placeCustomTile(5, 7, Box);
			placeCustomTile(6, 7, Box);
			placeCustomTile(7, 7, Box);
			//---------------------------------
			_boxesToPassLvl = 4;
			_actualBoxesPlaced = 0;
			//---------------------------------
			if (!_boxesAlreadyCreate) 
			{
				createBoxesPerLevel(Level_2);
				_boxesAlreadyCreate = true;
			}
			else {
				destroyLevlBoxes();
				createBoxesPerLevel(Level_2);
			}
			//---------------------------------
			_actualLevelLoaded = Level_2;
			//---------------------------------
		}
		//----------------
		void Grid::startLevel_3()
		{
			//------------------------
			cleanTileMap();
			//------------------------
			setBoxesSttngPerLvl(Level_3,2,3,2,6,7,3,7,6);
			//------------------------
			placeBasicWalls();	// BASICS WALLS
			//------------------------
			//CORNERS WALLS
			placeCustomWalls(1, 1, Top);
			placeCustomWalls(8, 1, Top);
			//----  WALL LEFT
			placeCustomWalls(3, 2, Rigth);
			placeCustomWalls(3, 3, Rigth);
			placeCustomWalls(3, 4, Top);
			placeCustomWalls(3, 6, Rigth);
			placeCustomWalls(3, 7, Rigth);
			placeCustomWalls(3, 8, Rigth);
			//----	WALL RIGTH
			placeCustomWalls(6, 2, Rigth);
			placeCustomWalls(6, 3, Rigth);
			placeCustomWalls(6, 4, Top);
			placeCustomWalls(6, 6, Rigth);
			placeCustomWalls(6, 7, Rigth);
			placeCustomWalls(6, 8, Rigth);
			//------------	POINT WHER WIN
			placeCustomTile(4, 1, Point);
			placeCustomTile(5, 1, Point);
			placeCustomTile(4, 8, Point);
			placeCustomTile(5, 8, Point);
			//------------  BOXES
			placeCustomTile(2, 3, Box);
			placeCustomTile(2, 6, Box);
			placeCustomTile(7, 3, Box);
			placeCustomTile(7, 6, Box);
			//---------------------------------
			_boxesToPassLvl = 4;
			_actualBoxesPlaced = 0;
			//---------------------------------
			if (!_boxesAlreadyCreate)
			{
				createBoxesPerLevel(Level_3);
				_boxesAlreadyCreate = true;
			}
			else {
				destroyLevlBoxes();
				createBoxesPerLevel(Level_3);
			}
			//------------------------
			_actualLevelLoaded = Level_3;
			//---------------------------------
		}
		//----------------
		void Grid::startLevel_4()
		{
			//------------------------
			cleanTileMap();
			//------------------------
			setBoxesSttngPerLvl(Level_4, 5, 6, 6, 6, 6, 7, 7, 7);
			//------------------------
			placeBasicWalls();
			//------------------------
			placeCustomWalls(2, 0, Rigth);
			placeCustomWalls(6, 0, Rigth);
			placeCustomWalls(2, 6, Rigth);
			placeCustomWalls(2, 7, Rigth);
			placeCustomWalls(2, 8, Rigth);
			//--
			placeCustomWalls(3, 6, Rigth);
			placeCustomWalls(3, 7, Rigth);
			placeCustomWalls(3, 8, Rigth);
			//--
			placeCustomWalls(4, 5, Rigth);
			placeCustomWalls(4, 6, Rigth);
			placeCustomWalls(4, 7, Rigth);
			placeCustomWalls(4, 8, Rigth);
			//---
			placeCustomWalls(6, 5, Top);
			placeCustomWalls(7, 5, Top);
			placeCustomWalls(8, 5, Top);
			//--
			placeCustomWalls(6, 1, Rigth);
			placeCustomWalls(6, 2, Top);
			//--
			placeCustomWalls(2, 1, Rigth);
			placeCustomWalls(2, 2, Rigth);
			placeCustomWalls(2, 3, Top);
			//------------
			placeCustomTile(1, 1, Point);
			placeCustomTile(1, 8, Point);
			placeCustomTile(7, 1, Point);
			placeCustomTile(8, 1, Point);
			//------------
			placeCustomTile(5, 6, Box);
			placeCustomTile(6, 6, Box);
			placeCustomTile(6, 7, Box);
			placeCustomTile(7, 7, Box);
			//---------------------------------
			_boxesToPassLvl = 4;
			_actualBoxesPlaced = 0;
			//------------------------
			if (!_boxesAlreadyCreate)
			{
				createBoxesPerLevel(Level_4);
				_boxesAlreadyCreate = true;
			}
			else {
				destroyLevlBoxes();
				createBoxesPerLevel(Level_4);
			}
			//------------------------
			_actualLevelLoaded = Level_4;
			//---------------------------------
		}
		//----------------
		void Grid::startLevel_5()
		{
			//------------------------
			cleanTileMap();
			//------------------------
			setBoxesSttngPerLvl(Level_5, 2, 2, 4, 2, 5, 2, 7, 2);
			//------------------------
			placeBasicWalls();
			//------------------------
			placeCustomWalls(1, 3, Top);
			placeCustomWalls(2, 3, Top);
			placeCustomWalls(3, 3, Top);
			//--
			placeCustomWalls(6, 3, Top);
			placeCustomWalls(7, 3, Top);
			placeCustomWalls(8, 3, Top);
			//--
			placeCustomWalls(3, 6, Rigth);
			placeCustomWalls(3, 7, Rigth);
			placeCustomWalls(3, 8, Rigth);
			placeCustomWalls(4, 6, Rigth);
			placeCustomWalls(4, 7, Rigth);
			placeCustomWalls(4, 8, Rigth);
			placeCustomWalls(5, 6, Rigth);
			placeCustomWalls(5, 7, Rigth);
			placeCustomWalls(5, 8, Rigth);
			placeCustomWalls(6, 6, Rigth);
			placeCustomWalls(6, 7, Rigth);
			placeCustomWalls(6, 8, Rigth);
			//----------
			placeCustomTile(1, 8, Point);
			placeCustomTile(2, 8, Point);
			placeCustomTile(7, 8, Point);
			placeCustomTile(8, 8, Point);
			//--------
			placeCustomTile(2, 2, Box);
			placeCustomTile(4, 2, Box);
			placeCustomTile(5, 2, Box);
			placeCustomTile(7, 2, Box);
			//---------------------------------
			_boxesToPassLvl = 4;
			_actualBoxesPlaced = 0;
			//------------------------
			if (!_boxesAlreadyCreate)
			{
				createBoxesPerLevel(Level_5);
				_boxesAlreadyCreate = true;
			}
			else {
				destroyLevlBoxes();
				createBoxesPerLevel(Level_5);
			}
			//------------------------
			_actualLevelLoaded = Level_5;
			//---------------------------------
		}
		//----------------
		bool Grid::playerCanPassToOtherLvL()
		{
			for (int i = 0; i < gridY; i++)
			{
				for (int j = 0; j < gridX; j++)
				{
					for (int k = 0; k < _maxBoxesForLevel; k++)
					{
						if (boxes[i] != NULL && tiles[j][i] != NULL)
						{
							if (tiles[j][i]->getTypeTile() == Point)
							{
								if (boxes[k]->getBOXGridX() == tiles[j][i]->getTileGridX() && boxes[k]->getBOXGridY() == tiles[j][i]->getTileGridY())
								{
									if (!boxes[k]->getIfIsPlaced() && _actualBoxesPlaced <= _boxesToPassLvl)
									{
										boxes[k]->setInPlacePoint(true);
										_actualBoxesPlaced++;
									}
								}
							}
						}
					}
				}
			}
			if (_actualBoxesPlaced == _boxesToPassLvl)
				return true;
			else
				return false;
		}
		//----------------
		void Grid::setBoxesSttngPerLvl(LEVEL_LOADED whatLevl, int box1ToGridX, int box1ToGridY, int box2ToGridX, int box2ToGridY,
			int box3ToGridX, int box3ToGridY, int box4ToGridX, int box4ToGridY)
		{
			if (tiles[box1ToGridX][box1ToGridY] != NULL)
			{
				b1[whatLevl].TILE_POSX = tiles[box1ToGridX][box1ToGridY]->getTileGridX();
				b1[whatLevl].TILE_POSY = tiles[box1ToGridX][box1ToGridY]->getTileGridY();
				b1[whatLevl].WORLD_POSX = tiles[box1ToGridX][box1ToGridY]->getTilePos().x;
				b1[whatLevl].WORLD_POSY = tiles[box1ToGridX][box1ToGridY]->getTilePos().y;
			}
			if (tiles[box2ToGridX][box2ToGridY] != NULL)
			{
				b2[whatLevl].TILE_POSX = tiles[box2ToGridX][box2ToGridY]->getTileGridX();
				b2[whatLevl].TILE_POSY = tiles[box2ToGridX][box2ToGridY]->getTileGridY();
				b2[whatLevl].WORLD_POSX = tiles[box2ToGridX][box2ToGridY]->getTilePos().x;
				b2[whatLevl].WORLD_POSY = tiles[box2ToGridX][box2ToGridY]->getTilePos().y;		
			}
			if (tiles[box3ToGridX][box3ToGridY] != NULL)
			{
				b3[whatLevl].TILE_POSX = tiles[box3ToGridX][box3ToGridY]->getTileGridX();
				b3[whatLevl].TILE_POSY = tiles[box3ToGridX][box3ToGridY]->getTileGridY();
				b3[whatLevl].WORLD_POSX = tiles[box3ToGridX][box3ToGridY]->getTilePos().x;
				b3[whatLevl].WORLD_POSY = tiles[box3ToGridX][box3ToGridY]->getTilePos().y;	
			}
			if (tiles[box4ToGridX][box4ToGridY] != NULL)
			{
				b4[whatLevl].TILE_POSX = tiles[box4ToGridX][box4ToGridY]->getTileGridX();
				b4[whatLevl].TILE_POSY = tiles[box4ToGridX][box4ToGridY]->getTileGridY();
				b4[whatLevl].WORLD_POSX = tiles[box4ToGridX][box4ToGridY]->getTilePos().x;
				b4[whatLevl].WORLD_POSY = tiles[box4ToGridX][box4ToGridY]->getTilePos().y;
			}
		}
		//----------------
		void Grid::destroyLevlBoxes()
		{
			for (int i = 0; i < _maxBoxesForLevel; i++)
			{
				if (boxes[i] != NULL)
					delete boxes[i];
				boxes[i] = NULL;
			}
		}
		//----------------
		void Grid::createBoxesPerLevel(LEVEL_LOADED whatLvl)
		{
			boxes[0] = new BoxTile(false, b1[whatLvl].WORLD_POSX, b1[whatLvl].WORLD_POSY, BOXES_SCALE, BOXES_SCALE, TYPE_TILE::Box, b1[whatLvl].TILE_POSX, b1[whatLvl].TILE_POSY);
			boxes[1] = new BoxTile(false, b2[whatLvl].WORLD_POSX, b2[whatLvl].WORLD_POSY, BOXES_SCALE, BOXES_SCALE, TYPE_TILE::Box, b2[whatLvl].TILE_POSX, b2[whatLvl].TILE_POSY);
			boxes[2] = new BoxTile(false, b3[whatLvl].WORLD_POSX, b3[whatLvl].WORLD_POSY, BOXES_SCALE, BOXES_SCALE, TYPE_TILE::Box, b3[whatLvl].TILE_POSX, b3[whatLvl].TILE_POSY);
			boxes[3] = new BoxTile(false, b4[whatLvl].WORLD_POSX, b4[whatLvl].WORLD_POSY, BOXES_SCALE, BOXES_SCALE, TYPE_TILE::Box, b4[whatLvl].TILE_POSX, b4[whatLvl].TILE_POSY);
		}
		//----------------
		Grid::Grid(Screen* w)
		{
			//------
			float genericScaleX = 62.0f;
			float genericScaleY = 62.0f;
			_boxesToPassLvl = 0;
			_actualBoxesPlaced = 0;
			//------
			loadTiles(genericScaleX, genericScaleY);
			//------
			float startPosX = (float)((w->getScreenWitdh() *0.5f) - ((genericScaleX * gridX) * 0.5f));
			float startPosY = (float)((w->getScreenHeigth()*0.5f) - ((genericScaleY * gridY) * 0.5f));
			//--------------------
			for (int i = 0; i < canLevels; i++)
			{
				//------
				b1[i].TILE_POSX = 0;
				b1[i].TILE_POSY = 0;
				b1[i].WORLD_POSX = 0;
				b1[i].WORLD_POSY = 0;
				//------
				b2[i].TILE_POSX = 0;
				b2[i].TILE_POSY = 0;
				b2[i].WORLD_POSX = 0;
				b2[i].WORLD_POSY = 0;
				//------
				b3[i].TILE_POSX = 0;
				b3[i].TILE_POSY = 0;
				b3[i].WORLD_POSX = 0;
				b3[i].WORLD_POSY = 0;
				//------
				b4[i].TILE_POSX = 0;
				b4[i].TILE_POSY = 0;
				b4[i].WORLD_POSX = 0;
				b4[i].WORLD_POSY = 0;
				//------
			}
			BOXES_SCALE = 50;

			for (int i = 0; i < gridY; i++)
			{
				for (int j = 0; j < gridX; j++)
				{
					tiles[j][i] = NULL;
				}
			}
			for (int i = 0; i < _maxBoxesForLevel; i++)
			{
				boxes[i] = NULL;
			}

			for (int i = 0; i < gridY; i++)
			{
				for (int j = 0; j < gridX; j++)
				{
					tiles[j][i] = new Tile((startPosX + (genericScaleX*j)), (startPosY + (genericScaleY*i)),
						genericScaleX, genericScaleY, TYPE_TILE::Floor, j, i);
				}
			}
			//--------------------
			_boxesAlreadyCreate = false;
			//--------------------
		}
		//----------------
		Grid::~Grid()
		{
			for (int i = 0; i < gridY; i++)
			{
				for (int j = 0; j < gridX; j++)
				{
					if(tiles[j][i]!=NULL)
						delete tiles[j][i];
					tiles[j][i] = NULL;
				}
			}
			//----
			for (int i = 0; i < _maxBoxesForLevel; i++)
			{
				if (boxes[i] != NULL)
					delete boxes[i];
				boxes[i] = NULL;
			}
			//----
			unloadTiles();
		}
		//----------------
		void Grid::checkBoxMoving(int cantBoxesActive,int xGrid, int yGrid, int& playerDirection, bool isOnTranslate)
		{
			switch (cantBoxesActive)
			{
			case 4:
				//---------------
				boxesMovement(0,1,2,3,xGrid,yGrid,playerDirection,isOnTranslate);
				//---------------
				boxesMovement(1,0,2,3,xGrid,yGrid,playerDirection, isOnTranslate);
				//---------------
				boxesMovement(2,1,0,3,xGrid,yGrid,playerDirection, isOnTranslate);
				//---------------
				boxesMovement(3,1,2,0,xGrid,yGrid,playerDirection, isOnTranslate);
				//---------------
				break;
			}
		}
		//----------------
		void Grid::boxesMovement(int actualBox, int otherBox1, int otherBox2, int otherBox3
			,int xGrid, int yGrid, int playerDirection, bool isOnTranslate)
		{

			if (boxes[actualBox] != NULL) {

				if (tiles[boxes[actualBox]->_gridPosXBox + 1][boxes[actualBox]->_gridPosYBox] != NULL &&
					tiles[boxes[actualBox]->_gridPosXBox - 1][boxes[actualBox]->_gridPosYBox] != NULL &&
					tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox + 1] != NULL &&
					tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox - 1] != NULL)
				{
					if (tiles[boxes[actualBox]->_gridPosXBox + 1][boxes[actualBox]->_gridPosYBox]->getTypeTile() == Box ||
						tiles[boxes[actualBox]->_gridPosXBox - 1][boxes[actualBox]->_gridPosYBox]->getTypeTile() == Box ||
						tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox + 1]->getTypeTile() == Box ||
						tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox - 1]->getTypeTile() == Box)
					{
						boxes[actualBox]->setInteractable(false);
					}
					else
					{
						boxes[actualBox]->setInteractable(true);
					}
				}
				if (!boxes[actualBox]->getOnSlide())
				{
					boxes[actualBox]->_gridPosXBox = boxes[actualBox]->toMoveX;
					boxes[actualBox]->_gridPosYBox = boxes[actualBox]->toMoveY;
					if(tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox]->getTypeTile() != Point)
						tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox]->setTypeTile(TYPE_TILE::Box);
				}
				if (xGrid == boxes[actualBox]->_gridPosXBox && yGrid == boxes[actualBox]->_gridPosYBox
					&& isOnTranslate)
				{
					tiles[boxes[actualBox]->_gridPosXBox][boxes[actualBox]->_gridPosYBox]->setTypeTile(TYPE_TILE::Floor);
					boxes[actualBox]->setOnSlide(true);
				}

				if (boxes[actualBox]->getOnSlide()) 
				{
					boxes[actualBox]->setPosTileMap(tiles, playerDirection);
				}
			}
		}
		//----------------
		void Grid::rotateTextureAndDraw(Texture2D& toRote, int X, int Y) {
			//-------------------
			switch (tiles[X][Y]->getOrientation())
			{
			case SPECIFICATION::Top:
				if (tiles[X][Y] != NULL)
					DrawTextureEx(toRote, tiles[X][Y]->getTilePos(), 0.0f, 1.0f, WHITE);
				break;
			case SPECIFICATION::Bot:
				if (tiles[X][Y] != NULL)
					DrawTextureEx(toRote, tiles[X][Y]->getTilePos(), 0.0f, 1.0f, WHITE);
				break;
			case SPECIFICATION::Left:
				if (tiles[X][Y] != NULL)
					DrawTextureEx(_whatTile[2], tiles[X][Y]->getTilePos(), 0.0f, 1.0f, WHITE);
				break;
			case SPECIFICATION::Rigth:
				if (tiles[X][Y] != NULL)
					DrawTextureEx(_whatTile[2], tiles[X][Y]->getTilePos(), 0.0f, 1.0f, WHITE);
				break;
			}
			//-------------------
		}
		//----------------
		void Grid::drawTilesGrid()
		{
			for (int  i = 0; i < gridY; i++)
			{
				for (int j = 0; j < gridX; j++)
				{
					//-------
					if (tiles[j][i] != NULL)
					{
						switch (tiles[j][i]->getTypeTile())
						{
						case Floor:
							DrawTextureEx(_whatTile[0], tiles[j][i]->getTilePos(), 0.0f, 1.0f, WHITE);
							break;
						case Box:
							DrawTextureEx(_whatTile[0], tiles[j][i]->getTilePos(), 0.0f, 1.0f, WHITE);
							break;
						case Wall:
							rotateTextureAndDraw(_whatTile[1], j, i);
							break;
						case Point:
							DrawTextureEx(_whatTile[3], tiles[j][i]->getTilePos(), 0.0f, 1.0f, WHITE);
							break;
						}
						//-------
						#if DEBUG
						tiles[j][i]->drawColliderTile();
						DrawTextRec(GetFontDefault(), FormatText(" %i,%i ", j, i), tiles[j][i]->getCollider(), 25, 1.0f, false, WHITE);
						#endif
						//-------
					}
				}
			}
			for (int i = 0; i < _maxBoxesForLevel; i++)
			{
				if(boxes[i]!=NULL)
					DrawTextureEx(_whatTile[4], boxes[i]->getTilePos(), 0.0f, 1.0f, BROWN);
			}
		}
		//----------------
		void Grid::loadTiles(float wTile, float hTile)
		{
			int castedWidth = (int)(wTile);
			int castedHeigth = (int)(hTile);
			Image resize;
			//----------------------------
			resize = LoadImage("res/assets/Tilegrid/floorTile.png");
			ImageResize(&resize, castedWidth, castedHeigth);
			_whatTile[0] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//----------------------------
			resize = LoadImage("res/assets/Tilegrid/wallTileTop.png");
			ImageResize(&resize, castedWidth, castedHeigth);
			_whatTile[1] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//----------------------------
			resize = LoadImage("res/assets/Tilegrid/wallTileSides.png");
			ImageResize(&resize, castedWidth, castedHeigth);
			_whatTile[2] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//----------------------------
			resize = LoadImage("res/assets/Tilegrid/pointPlace.png");
			ImageResize(&resize, castedWidth, castedHeigth);
			_whatTile[3] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//----------------------------
			resize = LoadImage("res/assets/Tilegrid/box.png");
			ImageResize(&resize, (int)(castedWidth*0.8f), (int)(castedHeigth*0.8f));
			_whatTile[4] = LoadTextureFromImage(resize);
			UnloadImage(resize);
		}
		//----------------
		void Grid::unloadTiles()
		{
			UnloadTexture(_whatTile[0]);
			UnloadTexture(_whatTile[1]);
			UnloadTexture(_whatTile[2]);
			UnloadTexture(_whatTile[3]);
			UnloadTexture(_whatTile[4]);
		}
		bool Grid::getCantMovePlayer()
		{
			for (int i = 0; i < 4; i++)
			{
				if (boxes[i] != NULL)
				{
					if (boxes[i]->getInteractable())
						return false;
					else 
						return true;
				}
			}
		}
		//----------------
	}
}