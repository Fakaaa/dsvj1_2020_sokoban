#include <iostream>

#include "box.h"

namespace Sokoban {
	namespace BoxHandle {
		//----------------
		BoxTile::BoxTile(bool is, float x, float y,
			float w, float h, TYPE_TILE type, 
			int gridPosX, int gridPosY) : Tile(x,y,w,h,type,gridPosX,gridPosY)
		{
			_isInteractable = is;
			_onSlide = false;
			_sheMovesTop = false;
			_sheMovesBot = false;
			_sheMovesLeft = false;
			_sheMovesRigth = false;
			_imInPlace = false;

			_gridPosXBox = gridPosX;
			_gridPosYBox = gridPosY;

			toMoveX = _gridPosXBox;
			toMoveY = _gridPosYBox;
			
			_toMoveWorldX = x;
			_toMoveWorldY = y;
		}
		BoxTile::BoxTile(TYPE_TILE type):Tile( type){ }
		//----------------
		BoxTile::~BoxTile(){}
		//----------------
		void BoxTile::setInPlacePoint(bool placed)
		{
			_imInPlace = placed;
		}
		//----------------
		void BoxTile::setInteractable(bool what)
		{
			_isInteractable = what;
		}
		void BoxTile::setOnSlide(bool itIs)
		{
			_onSlide = itIs;
		}
		void BoxTile::setBOXGRIDPosX(int xpos)
		{
			_gridPosXBox = xpos;
		}
		void BoxTile::setBOXGRIDPosY(int ypos)
		{
			_gridPosYBox = ypos;
		}
		void BoxTile::setBOXWorldPosX(float xpos)
		{
			_toMoveWorldX = xpos;
		}
		void BoxTile::setBOXWorldPosY(float ypos)
		{
			_toMoveWorldY = ypos;
		}
		void BoxTile::setToMoveX(int x)
		{
			if (toMoveY < 9)
				toMoveX = x;
		}
		void BoxTile::setToMoveY(int y)
		{
			toMoveY = y;
		}
		//----------------
		void BoxTile::setPosTileMap(Tile * tileGrid[10][10], int& playerDirecetion)
		{
			if (tileGrid[toMoveX][toMoveY] != NULL)
			{
				switch (playerDirecetion)
				{
				case 0:
					if (!_sheMovesTop && _onSlide)
					{
						if (toMoveY < 9 && tileGrid[toMoveX][toMoveY - 1]->getTypeTile() == Floor ||
							tileGrid[toMoveX][toMoveY - 1]->getTypeTile() == Point && 
							tileGrid[toMoveX][toMoveY - 2]->getTypeTile() != Box) 
							toMoveY -= 1;
						_sheMovesTop = true;
					}
					break;
				case 1:
					if (!_sheMovesBot && _onSlide)
					{
						if (toMoveY > 0 && tileGrid[toMoveX][toMoveY + 1]->getTypeTile() == Floor ||
							tileGrid[toMoveX][toMoveY + 1]->getTypeTile() == Point &&
							tileGrid[toMoveX][toMoveY + 2]->getTypeTile() != Box)
							toMoveY += 1;
						_sheMovesBot = true;
					}
					break;
				case 2:
					if (!_sheMovesLeft && _onSlide)
					{
						if (toMoveX < 9 && tileGrid[toMoveX - 1][toMoveY]->getTypeTile() == Floor ||
							tileGrid[toMoveX - 1][toMoveY]->getTypeTile() == Point &&
							tileGrid[toMoveX - 2][toMoveY]->getTypeTile() != Box)
							toMoveX -= 1;
						_sheMovesLeft = true;
					}
					break;
				case 3:
					if (!_sheMovesRigth && _onSlide)
					{
						if (toMoveX > 0 && tileGrid[toMoveX + 1][toMoveY]->getTypeTile() == Floor ||
							tileGrid[toMoveX + 1][toMoveY]->getTypeTile() == Point &&
							tileGrid[toMoveX + 2][toMoveY]->getTypeTile() != Box)
							toMoveX += 1;
						_sheMovesRigth = true;
					}
					break;
				}

				std::cout << "POSX grid " << toMoveX << std::endl;
				std::cout << "POSY grid " << toMoveY << std::endl;

				_toMoveWorldX = tileGrid[toMoveX][toMoveY]->getTilePos().x;
				_toMoveWorldY = tileGrid[toMoveX][toMoveY]->getTilePos().y;
				Tile::setPosGridTileX(toMoveX);
				Tile::setPosGridTileX(toMoveY);

				placBoxOnTile(_toMoveWorldX, _toMoveWorldY);
			}
		}
		//----------------
		void BoxTile::placBoxOnTile(float & newPosx, float & newPosy)
		{
			if (Tile::getTilePos().x != newPosx || Tile::getTilePos().y != newPosy && _onSlide)
			{
				Tile::setPosTile(newPosx , newPosy);
				std::cout << "X grid " << toMoveX << std::endl;
				std::cout << "Y grid " << toMoveY << std::endl;
				//std::cout << "ENTREO" << std::endl;
			}
			else {
				_sheMovesTop = false;
				_sheMovesBot = false;
				_sheMovesLeft = false;
				_sheMovesRigth = false;
				_onSlide = false;
				//Tile::setPosGridTileX(_toMoveX);
				//Tile::setPosGridTileX(_toMoveY);
			}
		}
		//----------------
	}
}