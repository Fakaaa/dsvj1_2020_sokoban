#include "uxInGame.h"

namespace Sokoban {
	namespace UX_Game_Handle {

		//------------------------
		UX_Menu::UX_Menu(Screen* window) : Menu()
		{
			_wantBackToMenu = false;
			_menuPauseActive = false;
			_canGoToNextLvl = false;
			_toNextLevel = false;
			_resetLevel = false;
			_resumeLvl = false;
			_UIActive = false;
			_type = UX_Game;

			if(window!=NULL)
				_pauseBg = {0.0f ,0.0f, (float)(window->getScreenWitdh()) ,(float)(window->getScreenHeigth()) };

			float posX = 0.0f; //
			float posY = 0.0f; //	
			float posX_2 = 0.0f; //
			float posY_2 = 0.0f; //		BUTTONS SETTINGS
			int scaleX_2 = 130;  //
			int scaleY_2 = 65;  //
			int scaleX = 195;  //
			int scaleY = 65;  //

			_pressEfectX = scaleX;
			_pressEfectY = scaleY;
			_pressEfectX_2 = scaleX_2;
			_pressEfectY_2 = scaleY_2;

			_auxScaleX = scaleX;
			_auxScaleY = scaleY;
			_auxScaleX_2 = scaleX_2;
			_auxScaleY_2 = scaleY_2;
			//-------------------------------
			if (window != NULL)
			{
				posX = ((window->getScreenWitdh() *0.5f) - (scaleX * 0.5f));
				posY = ((window->getScreenHeigth()*0.5f) - (scaleY * 0.5f));

				posX_2 = ((window->getScreenWitdh() *0.055f) - (scaleX_2 * 0.5f));
				posY_2 = ((window->getScreenHeigth()*0.025f) - (scaleY_2 * 0.5f));
			}
			//-------------------------------
			for (int  i = 0; i < cantButton; i++)
			{
				_UI[i] = NULL;
			}
			_typeButton = Resume;
			int j = 0;
			for (int i = 0; i < cantButton; i++)
			{
				if (i <= 1)
				{
					_UI[i] = new Button( posX, (posY + (120 * i)), scaleX, scaleY, _typeButton + j );
				}
				else if( i > 1 && i <= 3){
					if(i == 2)
						_UI[i] = new Button( posX_2, (posY_2 + (10*i)), scaleX_2, scaleY_2, _typeButton + j);
					else
						_UI[i] = new Button(posX_2, (posY_2 + (30 * i)), scaleX_2, scaleY_2, _typeButton + j);
				}
				else if (i > 3 && i < cantButton)
				{
					_UI[i] = new Button(posX_2, (posY_2 + (30 * i)), scaleX_2, scaleY_2, _typeButton + j);
				}
				j++;
			}
		}
		//------------------------
		UX_Menu::~UX_Menu()
		{
			for (int i = 0; i < cantButton; i++)
			{
				if (_UI[i] != NULL)
					delete _UI[i];
				_UI[i] = NULL;
			}
		}
		void UX_Menu::setPauseActive(bool what)
		{
			_menuPauseActive = what;
		}
		void UX_Menu::setUIActive(bool what)
		{
			_UIActive = what;
		}
		void UX_Menu::setWantBack(bool what)
		{
			_wantBackToMenu = what;
		}
		void UX_Menu::setCanPassToOtherLVL(bool can)
		{
			_canGoToNextLvl = can;
		}
		void UX_Menu::setGoToNextLvl(bool yes)
		{
			_toNextLevel = yes;
		}
		void UX_Menu::resetLevelDone()
		{
			_resetLevel = false;
		}
		void UX_Menu::setResetLevelManual(bool reset)
		{
			_resetLevel = reset;
		}
		bool UX_Menu::canGoToNextLevel()
		{
			if (_canGoToNextLvl && _toNextLevel)
				return true;
			else
				return false;
		}
		//------------------------
		bool UX_Menu::getWantBackMenu()
		{
			if (_wantBackToMenu)
				return true;
			else
				return false;
		}
		bool UX_Menu::getResetLevel()
		{
			if (_resetLevel)
				return true;
			else
				return false;
		}
		bool UX_Menu::getResumeLevel()
		{
			if (_resumeLvl)
				return true;
			else
				return false;
		}
		//--------------------------
		bool UX_Menu::getPauseActive()
		{
			if (_menuPauseActive)
				return true;
			else
				return false;
		}
		//--------------------------
		void UX_Menu::inputsMouse(SoundsController* handle1, SoundsController* handle2)
		{
			if (_UI[0] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[0]->getCollider())) {
					_UI[0]->setScaleX(_pressEfectX + 20);
					_UI[0]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[2]->setColorText(YELLOW);
						_menuPauseActive = false;
					}
				}
				else {
					_UI[0]->setScaleX(_auxScaleX);
					_UI[0]->setScaleY(_auxScaleY);
					_UI[0]->setColorText(WHITE);
				}
			}
			if (_UI[1] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[1]->getCollider())) {
					_UI[1]->setScaleX(_pressEfectX + 20);
					_UI[1]->setScaleY(_pressEfectY + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[2]->setColorText(YELLOW);
						_wantBackToMenu = true;
					}
				}
				else {
					_UI[1]->setScaleX(_auxScaleX);
					_UI[1]->setScaleY(_auxScaleY);
					_UI[1]->setColorText(WHITE);
				}
			}
			if (_UI[2] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[2]->getCollider())) {
					_UI[2]->setScaleX(_pressEfectX_2 + 20);
					_UI[2]->setScaleY(_pressEfectY_2 + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[2]->setColorText(YELLOW);
						_resetLevel = true;
					}
				}
				else {
					_UI[2]->setScaleX(_auxScaleX_2);
					_UI[2]->setScaleY(_auxScaleY_2);
					_UI[2]->setColorText(WHITE);
				}
			}
			//--------------------
			if (_UI[3] != NULL)
			{
				if (CheckCollisionPointRec(Menu::getMousePos(), _UI[3]->getCollider())) {
					_UI[3]->setScaleX(_pressEfectX_2 + 20);
					_UI[3]->setScaleY(_pressEfectY_2 + 20);
					if (handle1 != NULL)
					{
						handle1->stopSound();
						handle1->playSound();
					}
					if (Menu::getMousePress())
					{
						if (handle2 != NULL)
						{
							handle2->stopSound();
							handle2->playSound();
						}
						_UI[3]->setColorText(YELLOW);
						_menuPauseActive = true;
					}
				}
				else {
					_UI[3]->setScaleX(_auxScaleX_2);
					_UI[3]->setScaleY(_auxScaleY_2);
					_UI[3]->setColorText(WHITE);
				}
			}
			//--------------------
			if (_canGoToNextLvl)
			{
				if (_UI[4] != NULL)
				{
					if (CheckCollisionPointRec(Menu::getMousePos(), _UI[4]->getCollider())) {
						_UI[4]->setScaleX(_pressEfectX + 20);
						_UI[4]->setScaleY(_pressEfectY + 20);
						if (handle1 != NULL)
						{
							handle1->stopSound();
							handle1->playSound();
						}
						if (Menu::getMousePress())
						{
							if (handle2 != NULL)
							{
								handle2->stopSound();
								handle2->playSound();
							}
							_UI[4]->setColorText(YELLOW);
							_menuPauseActive = false;
							_toNextLevel = true; 
						}
					}
					else {
						_UI[4]->setScaleX(_auxScaleX);
						_UI[4]->setScaleY(_auxScaleY);
						_UI[4]->setColorText(WHITE);
					}
				}
			}
			//--------------------
			Menu::updateMousePos();
			//--------------------
		}
		void UX_Menu::draw()
		{
			if (_menuPauseActive)
			{
				DrawRectangleRec(_pauseBg, ALPHAPAUSE);

				for (int i = 0; i < cantButton - 3; i++)
				{
					if (_UI[i] != NULL)
					{
						DrawTextureEx(_UI[i]->getBtn(), _UI[i]->getPos(), 0.0f, 1.0f, LIGHTGRAY);
						DrawTextRec(_UI[i]->getFontTextBtn(), _UI[i]->getTextBtn(), _UI[i]->getCollider(), _UI[i]->getFontSize(),
							_UI[i]->getSpacing(), _UI[i]->getWordlWrap(), _UI[i]->getColorText());
						#if DEBUG
						DrawRectangleLinesEx(_UI[i]->getCollider(), 1, GREEN);
						#endif
					}
				}
			}
			if(_UIActive) {
				for (int i = 2; i < cantButton-1; i++)
				{
					if (_UI[i] != NULL && !_canGoToNextLvl)
					{
						DrawTextureEx(_UI[i]->getBtn(), _UI[i]->getPos(), 0.0f, 1.0f, LIGHTGRAY);
						DrawTextRec(_UI[i]->getFontTextBtn(), _UI[i]->getTextBtn(), _UI[i]->getCollider(), _UI[i]->getFontSize(),
							_UI[i]->getSpacing(), _UI[i]->getWordlWrap(), _UI[i]->getColorText());
						#if DEBUG
						DrawRectangleLinesEx(_UI[i]->getCollider(), 1, GREEN);
						#endif
					}
					else if (_UI[4] != NULL && _canGoToNextLvl)
					{
						DrawTextureEx(_UI[4]->getBtn(), _UI[4]->getPos(), 0.0f, 1.0f, LIGHTGRAY);
						DrawTextRec(_UI[4]->getFontTextBtn(), _UI[4]->getTextBtn(), _UI[4]->getCollider(), _UI[4]->getFontSize(),
							_UI[4]->getSpacing(), _UI[4]->getWordlWrap(), _UI[4]->getColorText());
						#if DEBUG
						DrawRectangleLinesEx(_UI[4]->getCollider(), 1, GREEN);
						#endif
					}
				}			
			}
			//-----------
		}
		bool UX_Menu::getMenuOn()
		{
			if (_UIActive)
				return true;
			else
				return false;
		}
		//------------------------
	}
}