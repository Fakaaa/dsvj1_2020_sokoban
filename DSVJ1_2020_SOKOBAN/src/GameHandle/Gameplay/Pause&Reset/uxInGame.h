#ifndef UXINGAME_H
#define UXINGAME_H

#include "GameHandle/Menu/menuHandle.h"
#include "GameHandle/Menu/Buttons/buttons.h"
using namespace Sokoban;
using namespace MenuHandle;
using namespace ButtonsHandle;

#define ALPHAPAUSE CLITERAL(Color){0,0,0,155}

namespace Sokoban {
	namespace UX_Game_Handle {
		//------------------
		const int cantButton = 5;
		//------------------
		class UX_Menu : public Menu
		{
		private:
			Button* _UI[cantButton];
			Rectangle _pauseBg;

			bool _wantBackToMenu;
			bool _menuPauseActive;
			bool _UIActive;
			bool _resetLevel;
			bool _resumeLvl;

			bool _canGoToNextLvl;
			bool _toNextLevel;
			
			int _pressEfectX_2;
			int _pressEfectY_2;
			int _auxScaleX_2;
			int _auxScaleY_2;
		public:
			TYPE_BTNS _typeButton;

			UX_Menu(Screen* window);
			~UX_Menu();
			void setPauseActive(bool what);
			void setUIActive(bool what);
			void setWantBack(bool what);
			void setCanPassToOtherLVL(bool can);
			void setGoToNextLvl(bool yes);
			void resetLevelDone();
			void setResetLevelManual(bool reset);
			bool canGoToNextLevel();
			bool getWantBackMenu();
			bool getResetLevel();
			bool getResumeLevel();
			bool getPauseActive();

			virtual void inputsMouse(SoundsController* handle1, SoundsController* handle2);
			virtual void draw();
			virtual bool getMenuOn();
			virtual int getTypeMenu() { return _type; }
		};
		//------------------
	}
}
#endif // !UXINGAME_H