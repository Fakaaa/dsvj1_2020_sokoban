#ifndef PLAYER_H
#define PLAYER_H

#define MAX_FRAME_SPEED 16
#define MIN_FRAME_SPEED 3

#include "raylib.h"
#include "Screen/screen.h"
#include "GameHandle/Gameplay/GridMap/grid.h"
using namespace Sokoban;
using namespace ScreenHandle;
using namespace GridHandle;

namespace Sokoban {
	namespace PlayerHandle {
		//------------------
		const int directions = 4;
		const int cantFramesUpDown = 8;
		const int cantFramesSides = 7;
		//------------------
		enum ANIMSTATE
		{
			Up,
			Down,
			Left,
			Right
		};
		//------------------
		class Player
		{
		private:
			float My_frameCounter;
			float My_currentFrame;
			float My_frameSpeed;
			//------------------
			Vector2 _posWorld;
			int _posXTileMap;
			int _posYTileMap;
			float _scaleX;
			float _scaleY_1;
			float _scaleY_2;
			int _boxesToPlace;
			//------------------
			bool _onTranslate;
			bool _GoUp;
			bool _GoDown;
			bool _GoLeft;
			bool _GoRigth;
			float toMoveWorldX;
			float toMoveWorldY;
			int toMoveTileX;
			int toMoveTileY;
			//------------------
			int _currentAnimation;
			int _currentHeigthFrame;
			Texture2D _animation[directions];
			Rectangle _frameRec;
			int _direction;
			//------------------
		protected:
			void calcFrameAnimation(Screen* w);
		public:
			//-----------------------------
			Player(Screen* w, int posGridX, int posGridY, float worldX,float worldY);
			~Player();
			void setPosGridX(int gridX);
			void setPosGridY(int gridY);
			void setPosWorld(Vector2 position);
			//-----------------------------
			void setPosTileMap(Tile* tileGrid[gridX][gridY], bool iCantMove);
			void placPlayerOnTile(float& newPosx, float& newPosy);
			//-----------------------------
			void inputs();
			void update(Screen* w, Tile* tilesGrid[gridX][gridY], bool iCantMove);
			void draw();
			//-----------------------------
			void loadSpriteSheets();
			void unloadSpriteSheets();
			//-----------------------------
			int getDirection() { return _direction;  }
			float getToTranslateX() { return toMoveWorldX; }
			float getToTranslateY() { return toMoveWorldY; }
			int getPlayerGridX() { return _posXTileMap; }
			int getPlayerGridY() { return _posYTileMap; }
			Vector2 getPosWorld() { return _posWorld; }
			bool wantToMove() { return _onTranslate; }
		};
		//------------------
	}
}
#endif // !PLAYER_H