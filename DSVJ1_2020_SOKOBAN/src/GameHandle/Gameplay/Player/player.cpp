#include "player.h"
#include <iostream>

namespace Sokoban {
	namespace PlayerHandle {
		//------------------------------
		int frameSpeed = 12;
		//------------------------------
		void Player::calcFrameAnimation(Screen* w)
		{
			//-------------
			My_frameCounter++;
			//-------------
			if (My_frameCounter >= (w->getFPSLimits() / My_frameSpeed))
			{
				My_frameCounter = 0;
				My_currentFrame++;

				if (My_currentFrame >= cantFramesUpDown)
					My_currentFrame = 0;

				_frameRec.y = (float)(My_currentFrame)*(float)(_currentHeigthFrame);
			}
			if (My_frameSpeed >= MAX_FRAME_SPEED)My_frameSpeed = MAX_FRAME_SPEED;
			else if(My_frameSpeed <= MIN_FRAME_SPEED)My_frameSpeed = MIN_FRAME_SPEED;
			//-------------
		}
		//------------------------------
		Player::Player(Screen* w, int posGridX, int posGridY, float worldX, float worldY)
		{
			My_currentFrame = 0;
			My_frameCounter = 0;
			My_frameSpeed = frameSpeed;
			//--------------------------
			_posWorld = { worldX, worldY };
			_posXTileMap = posGridX;
			_posYTileMap = posGridY;
			//--------------------------
			_scaleX = 32.0f;
			_scaleY_1 = (float)(384 / cantFramesUpDown);
			_scaleY_2 = (float)(336 / cantFramesSides);
			_onTranslate = false;
			_GoUp = false;
			_GoDown = false;
			_GoLeft = false;
			_GoRigth = false;
			toMoveWorldX = 0.0f;
			toMoveWorldY = 0.0f;
			//--------------------------
			loadSpriteSheets();
			//--------------------------
			_currentAnimation = ANIMSTATE::Down;
			_direction = Down;
			_currentHeigthFrame = _scaleY_1;
			//--------------------------
			_frameRec = { 0.0f,0.0f,  _scaleX , _scaleY_1};
		}
		//------------------------------
		Player::~Player()
		{
			unloadSpriteSheets();
		}
		//------------------------------
		void Player::setPosGridX(int gridX)
		{
			_posXTileMap = gridX;
		}
		//------------------------------
		void Player::setPosGridY(int gridY)
		{
			_posYTileMap = gridY;
		}
		//------------------------------
		void Player::setPosWorld(Vector2 position)
		{
			_posWorld = position;
		}
		//------------------------------
		void Player::setPosTileMap(Tile* tileGrid[gridX][gridY], bool iCantMove)
		{

			if (tileGrid[toMoveTileX][toMoveTileY] != NULL)
			{
				if (_GoDown)
				{
					if (_GoDown && tileGrid[toMoveTileX][toMoveTileY + 1] != NULL)
					{
						if (tileGrid[toMoveTileX][toMoveTileY + 1]->getTypeTile() == Floor)
						{
							if (toMoveTileY < gridY - 1)
								toMoveTileY += 1;
							if (toMoveTileY == gridY - 1)
								toMoveTileY = toMoveTileY;
							_GoDown = false;
						}
					}
					if(_GoDown && tileGrid[toMoveTileX][toMoveTileY + 1] != NULL && tileGrid[toMoveTileX][toMoveTileY + 2]!=NULL)
					{
						if (tileGrid[toMoveTileX][toMoveTileY + 1]->getTypeTile() == Box &&
							tileGrid[toMoveTileX][toMoveTileY + 2]->getTypeTile() == Floor  ||
							tileGrid[toMoveTileX][toMoveTileY + 2]->getTypeTile() == Point )
						{
							if (toMoveTileY < gridY - 1)
								toMoveTileY += 1;
							if (toMoveTileY == gridY - 1)
								toMoveTileY = toMoveTileY;
							_GoDown = false;
						}
					}
				}
				if (_GoUp)
				{
					if (_GoUp && tileGrid[toMoveTileX][toMoveTileY - 1] != NULL)
					{
						if (tileGrid[toMoveTileX][toMoveTileY - 1]->getTypeTile() == Floor)
						{
							if (toMoveTileY > 0)
								toMoveTileY -= 1;
							if (toMoveTileY == 0)
								toMoveTileY = toMoveTileY;
							_GoUp = false;
						}
					}
					if(_GoUp && tileGrid[toMoveTileX][toMoveTileY - 1] != NULL && tileGrid[toMoveTileX][toMoveTileY - 2]!=NULL)
					{
						if (tileGrid[toMoveTileX][toMoveTileY - 1]->getTypeTile() ==  Box &&
							tileGrid[toMoveTileX][toMoveTileY - 2]->getTypeTile() == Floor ||
							tileGrid[toMoveTileX][toMoveTileY - 2]->getTypeTile() == Point)
						{
							if (toMoveTileY > 0)
								toMoveTileY -= 1;
							if (toMoveTileY == 0)
								toMoveTileY = toMoveTileY;
							_GoUp = false;
						}
					}
				}
				if (_GoLeft)
				{
					if (_GoLeft && tileGrid[toMoveTileX - 1][toMoveTileY] != NULL)
					{
						if (Floor == tileGrid[toMoveTileX - 1][toMoveTileY]->getTypeTile())
						{
							if (toMoveTileX > 0)
								toMoveTileX -= 1;
							if (toMoveTileX == 0)
								toMoveTileX = toMoveTileX;
							_GoLeft = false;
						}
					}
					if (toMoveTileX != 1 && _GoLeft && tileGrid[toMoveTileX - 1][toMoveTileY] != NULL && tileGrid[toMoveTileX - 2][toMoveTileY] != NULL)
					{
						if (Box == tileGrid[toMoveTileX - 1][toMoveTileY]->getTypeTile() &&
							Floor == tileGrid[toMoveTileX - 2][toMoveTileY]->getTypeTile() ||
							Point == tileGrid[toMoveTileX - 2][toMoveTileY]->getTypeTile())
						{
							if (toMoveTileX > 0)
								toMoveTileX -= 1;
							if (toMoveTileX == 0)
								toMoveTileX = toMoveTileX;
							_GoLeft = false;
						}
					}
				}
				if (_GoRigth)
				{
					if (_GoRigth && tileGrid[toMoveTileX + 1][toMoveTileY] != NULL)
					{
						if (Floor == tileGrid[toMoveTileX + 1][toMoveTileY]->getTypeTile())
						{
							if (toMoveTileX < gridX - 1)
								toMoveTileX += 1;
							if (toMoveTileX == gridX - 1)
								toMoveTileX = toMoveTileX;
							_GoRigth = false;
						}
					}
					if (toMoveTileX != gridX- 2 && _GoRigth && tileGrid[toMoveTileX + 1][toMoveTileY] != NULL && tileGrid[toMoveTileX + 2][toMoveTileY] != NULL)
					{
						if (Box == tileGrid[toMoveTileX + 1][toMoveTileY]->getTypeTile() &&
							Floor == tileGrid[toMoveTileX + 2][toMoveTileY]->getTypeTile() ||
							Point == tileGrid[toMoveTileX + 2][toMoveTileY]->getTypeTile())
						{
							if (toMoveTileX < gridX - 1)
								toMoveTileX += 1;
							if (toMoveTileX == gridX - 1)
								toMoveTileX = toMoveTileX;
							_GoRigth = false;
						}
					}
				}

				toMoveWorldX = tileGrid[toMoveTileX][toMoveTileY]->getTilePos().x;
				toMoveWorldY = tileGrid[toMoveTileX][toMoveTileY]->getTilePos().y;

				placPlayerOnTile(toMoveWorldX, toMoveWorldY);
			}
		}
		//------------------------------
		void Player::placPlayerOnTile(float& newPosx,float& newPosy)
		{
			if (_posWorld.x != newPosx || _posWorld.y != newPosy && _onTranslate)
			{
				_posWorld.x = newPosx;
				_posWorld.y = newPosy;
				_posXTileMap = toMoveTileX;
				_posYTileMap = toMoveTileY;
			}
			else
			{
				_onTranslate = false;
			}
		}
		//------------------------------
		void Player::inputs()
		{
			//-------------
			if (IsKeyPressed(KEY_DOWN)) 
			{
				_currentAnimation = ANIMSTATE::Down;
				_direction = Down;
				if (toMoveTileY < gridY - 1) {
					_onTranslate = true;
					_GoDown = true;
				}
			}
			else if (IsKeyPressed(KEY_UP)) 
			{
				_currentAnimation = ANIMSTATE::Up;
				_direction = Up;
				if (toMoveTileY > 0){
					_onTranslate = true;
					_GoUp = true;
				}
			}
			else if (IsKeyPressed(KEY_LEFT))
			{
				_currentAnimation = ANIMSTATE::Left;
				_direction = Left;
				if (toMoveTileX > 0) {
					_onTranslate = true;
					_GoLeft = true;
				}
			}
			else if (IsKeyPressed(KEY_RIGHT))
			{
				_currentAnimation = ANIMSTATE::Right;
				_direction = Right;
				if (toMoveTileX < gridX - 1) {
					_onTranslate = true;
					_GoRigth = true;
				}
			}
			//-------------
			if (IsKeyPressed(KEY_R))	//RESET TRANSLATE
				_onTranslate = false;
			//-------------
			if (_currentAnimation == (ANIMSTATE::Down || ANIMSTATE::Up))
				_currentHeigthFrame = _scaleY_1;
			else if(_currentAnimation == (ANIMSTATE::Right || ANIMSTATE::Left))
				_currentHeigthFrame = _scaleY_2;
			//-------------
		}
		//------------------------------
		void Player::update(Screen* w, Tile* tilesGrid[gridX][gridY], bool iCantMove)
		{
			//-------------
			//std::cout << "POS X TILE:" << toMoveTileX << std::endl;
			//std::cout << "POS Y TILE:" << toMoveTileY << std::endl;
			//-------------
			if (!_onTranslate) My_frameSpeed = 0;
			else My_frameSpeed = frameSpeed;
			//-------------
			calcFrameAnimation(w);
			//-------------
			if (!_onTranslate)
			{
				toMoveTileX = _posXTileMap;
				toMoveTileY = _posYTileMap;
			}
			else if(_onTranslate) setPosTileMap(tilesGrid, iCantMove);
			//-------------
		}
		//------------------------------
		void Player::draw()
		{
			//-------
			DrawTextureRec(_animation[_currentAnimation], _frameRec, _posWorld, WHITE);
			//-------
		}
		//------------------------------
		void Player::loadSpriteSheets()
		{
			//-----------------------
			Image resize;
			//-----------------------
			resize = LoadImage("res/assets/Player/down.png");
			ImageResize(&resize, (int)(_scaleX), (int)((_scaleY_1 * cantFramesUpDown)));
			_animation[Down] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//-----------------------
			resize = LoadImage("res/assets/Player/up.png");
			ImageResize(&resize, (int)(_scaleX), (int)((_scaleY_1 * cantFramesUpDown)));
			_animation[Up] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//-----------------------
			resize = LoadImage("res/assets/Player/left.png");
			ImageResize(&resize, (int)(_scaleX), (int)((_scaleY_2 * cantFramesSides)));
			_animation[Left] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//-----------------------
			resize = LoadImage("res/assets/Player/right.png");
			ImageResize(&resize, (int)(_scaleX), (int)((_scaleY_2 * cantFramesSides)));
			_animation[Right] = LoadTextureFromImage(resize);
			UnloadImage(resize);
			//-----------------------
		}
		//------------------------------
		void Player::unloadSpriteSheets()
		{
			UnloadTexture(_animation[Up]);
			UnloadTexture(_animation[Down]);
			UnloadTexture(_animation[Left]);
			UnloadTexture(_animation[Right]);
		}
		//------------------------------
	}
}