#ifndef MUSIC_H
#define MUSIC_H

#include "raylib.h"

namespace Sokoban {
	namespace MusicHanlde {
		//----------
		class MusicController
		{
		private:
			Music _song;
			float _volume;
		public:
			MusicController(const char* pathSong);
			~MusicController();
			bool getIfIsPlaying();
			void loadSongs(const char* pathSong);
			void unloadSongs();
			void playSong();
			void updateMusic();
			void stopSong();
		};
		//----------
	}
}
#endif // !MUSIC_H