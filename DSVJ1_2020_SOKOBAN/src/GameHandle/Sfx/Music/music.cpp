#include "music.h"

namespace Sokoban {
	namespace MusicHanlde {

		//---------------------
		MusicController::MusicController(const char* pathSong)
		{
			_volume = 0.2f;
			SetMasterVolume(_volume);
			loadSongs(pathSong);
		}
		//---------------------
		MusicController::~MusicController()
		{
			CloseAudioDevice();
		}
		bool MusicController::getIfIsPlaying()
		{
			if (IsMusicPlaying(_song))
				return true;
			else
				return false;
		}
		//---------------------
		void MusicController::loadSongs(const char* pathSong)
		{
			//---------------
			_song = LoadMusicStream(pathSong);
			//---------------
		}
		//---------------------
		void MusicController::unloadSongs()
		{
			//---
			UnloadMusicStream(_song);
			//---
		}
		//---------------------
		void MusicController::playSong()
		{
			if (!IsMusicPlaying(_song))
				PlayMusicStream(_song);
		}
		//---------------------
		void MusicController::updateMusic()
		{
			if (IsMusicPlaying(_song))
				UpdateMusicStream(_song);
		}
		//---------------------
		void MusicController::stopSong()
		{
			if (IsMusicPlaying(_song))
				StopMusicStream(_song);
		}
		//---------------------
	}
}