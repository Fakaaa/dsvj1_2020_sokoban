#include "sounds.h"

namespace Sokoban {
	namespace SoundsHandle {

		//-----------------------
		void SoundsController::loadSounds(const char * filePath)
		{
			_sound = LoadSound(filePath);
		}
		void SoundsController::unloadSounds()
		{
			UnloadSound(_sound);
		}
		//-----------------------
		SoundsController::SoundsController(const char * filePath)
		{
			loadSounds(filePath);
			_volume = 0.5f;
			SetSoundVolume(_sound, _volume);
		}
		//-----------------------
		SoundsController::~SoundsController()
		{
			unloadSounds();
		}
		//-----------------------
		void SoundsController::playSound()
		{
			if (!IsSoundPlaying(_sound))
				PlaySound(_sound);
		}
		void SoundsController::playSound(Sound whatSound)
		{
			if (!IsSoundPlaying(whatSound))
				PlaySound(whatSound);
		}
		//-----------------------
		void SoundsController::stopSound()
		{
			if (IsSoundPlaying(_sound))
				StopSound(_sound);
		}
		void SoundsController::stopSound(Sound whatSound)
		{
			if (IsSoundPlaying(whatSound))
				StopSound(whatSound);
		}
		//-----------------------
		void SoundsController::setVolumeSound(float volum)
		{
			SetSoundVolume(_sound, volum);
		}
		//-----------------------
	}
}