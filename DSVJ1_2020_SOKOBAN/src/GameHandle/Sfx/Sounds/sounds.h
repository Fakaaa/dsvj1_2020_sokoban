#ifndef SOUND_H
#define SOUND_H

#include "raylib.h"

namespace Sokoban {
	namespace SoundsHandle {
		//-------------
		class SoundsController
		{
		private:
			Sound _sound;
			float _volume;
		protected:
			void loadSounds(const char* filePath);
			void unloadSounds();
		public:
			SoundsController(const char * filePath);
			~SoundsController();
			Sound getSound() { return _sound; }
			void playSound();
			void playSound(Sound whatSound);
			void stopSound();
			void stopSound(Sound whatSound);
			void setVolumeSound(float volum);
		};
		//-------------
	}
}
#endif // !SOUND_H