#ifndef GAMEHANDLE_H
#define GAMEHANDLE_H

#include "raylib.h"
#include "Gameplay/GridMap/grid.h"
#include "Screen/screen.h"
#include "Gameplay/Player/player.h"
#include "Menu/mainMenu.h"
#include "Menu/menuHandle.h"
#include "Menu/LvlMenu/lvlMenu.h"
#include "Gameplay/Pause&Reset/uxInGame.h"
#include "Menu/Credits/credits.h"
#include "GameHandle/Sfx/Music/music.h"
#include "Sfx/Sounds/sounds.h"
using namespace Sokoban;
using namespace GridHandle;
using namespace ScreenHandle;
using namespace PlayerHandle;
using namespace MenuHandle;
using namespace MainMenuHanlde;
using namespace LevelHanlde;
using namespace UX_Game_Handle;
using namespace MusicHanlde;
using namespace SoundsHandle;
using namespace CreditsHandlde;

namespace Sokoban {
	namespace GameHandle {
		//---------------------
		const int menuSounds = 2;
		//---------------------
		enum SOUNDS_MENU
		{
			OverBtn,
			Click
		};
		//---------------------
		class GameHanlde
		{
		private:
			bool _onGame;
			bool _gamePaused;

			bool _levelCharge;
			bool _levelChoosed;
			int _actualLevel;

			MusicController* _gameplaySong;
			MusicController* _menuSong;
			SoundsController* _playerMove;
			SoundsController* _menuSfx[menuSounds];

			Menu* _UI_Menus[cantMaxMenus];
			Grid* _tileGrid;
			Player* _pj;

			int _directionPj;
		public:
			GameHanlde(Screen* window);
			~GameHanlde();
			void resetPlayerPos(int tileX, int tileY);
			void chargeLevel(int whatLevel);
			bool getGameClosed() { return _onGame; }
			void initAll(Screen* window);
			void updateAll(Screen* window);
			void drawAll();
			void inputs();
			void deinitAll();
		};
		//---------------------
	}
}
#endif // !GAMEHANDLE_H