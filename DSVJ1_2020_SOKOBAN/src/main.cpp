#include <iostream>
#include "GameLoop/gameLoop.h"

using namespace Sokoban;

void main() {
	GameLoop::Game* g = new GameLoop::Game();
	g->Play();
	if (g != NULL)
		delete g;
}