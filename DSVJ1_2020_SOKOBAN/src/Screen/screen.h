#ifndef SCREEN_H
#define SCREEN_H

#include <iostream>
#include <string>
#include "raylib.h"

using namespace std;

namespace Sokoban{
	namespace ScreenHandle {
		//-------------
		class Screen
		{
		private:
			int _screenWidth;
			int _screenHeigth;

			int _FPSLimit;

			const char* _windowName;
		public:
			Screen(int width, int heigth, const char* windowName);
			~Screen();
			void setScreenWidth(int width);
			void setScreenHeigth(int heigth);
			int getScreenWitdh() { return _screenWidth; }
			int getScreenHeigth() { return _screenHeigth; }
			void setNewResolution(int width, int heigth);
			const char* getNameWindow() { return _windowName; }
			int getFPSLimits() { return _FPSLimit; }
		};
		//-------------
	}
}
#endif // !SCREEN_H