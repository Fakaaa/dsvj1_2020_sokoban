#include "screen.h"

namespace Sokoban {
	namespace ScreenHandle {
		//-----------------
		Screen::Screen(int width, int heigth, const char* windowName) {
			_screenWidth = width;
			_screenHeigth = heigth;
			_FPSLimit = 120;
			SetTargetFPS(_FPSLimit);
			_windowName = windowName;
			InitWindow(width, heigth, windowName);
		}
		//-----------------
		Screen::~Screen() {	}
		//-----------------
		void Screen::setScreenWidth(int width)
		{
			_screenWidth = width;
		}
		//-----------------
		void Screen::setScreenHeigth(int heigth)
		{
			_screenHeigth = heigth;
		}
		//-----------------
		void Screen::setNewResolution(int width, int heigth)
		{
			SetWindowSize(width, heigth);
		}
		//-----------------
	}
}